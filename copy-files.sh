#!/bin/bash

source variables.sh

echo "Android folder:"
read androidFolder
echo "Copy bb_info? y or n"
read n

cp ./$androidFolder/qemu.log ./$androidTraces/qemu.log
if [ $n = "y" ]; then
	cp ./$androidFolder/bb_info.txt ./$androidTraces/bb_info.txt
fi

while read line
do
	IFS=";"
	set -- $line
	echo "$2;$3;$4" >> ./"$androidTraces"/"$finalTimeFile"
done <./$androidTraces/$timeInfoFile
#rm ./$androidTraces/$timeInfoFile


