#!/bin/bash

echo "Benchmarks Java:"
echo "1 - scimark fft"
echo "2 - scimark lu"
echo "3 - scimark monte carlo"
echo "4 - scimark sor"
echo "5 - scimark sparse"
echo "6 - serial"
echo "7 - crypto aes"
echo "8 - crypto rsa"
echo "9 - crypto signverify"
echo "10 - compress"
echo "11 - mpegaudio"
echo "12 - sqlite"
echo "-----------------------"
echo "100 - back button"
echo ""
echo "Option:"
read n

adb_cmd="./adb shell am start -n com.android.spec."
activity="/com.android.spec.SpecActivity"
cmd=""

case $n in
	1) cmd=$adb_cmd"fft"$activity;;
	2) cmd=$adb_cmd"lu"$activity;;
	3) cmd=$adb_cmd"montecarlo"$activity;;
	4) cmd=$adb_cmd"sor"$activity;;
	5) cmd=$adb_cmd"sparse"$activity;;
	6) cmd=$adb_cmd"serial"$activity;;
	7) cmd=$adb_cmd"aes"$activity;;
	8) cmd=$adb_cmd"rsa"$activity;;
	9) cmd=$adb_cmd"signverify"$activity;;
	10) cmd=$adb_cmd"compress"$activity;;
	11) cmd=$adb_cmd"mpegaudio"$activity;;
	12) cmd="./adb shell am start -n org.garret.bench/org.garret.bench.Benchmark";;
	100) cmd="./adb shell input keyevent 4";;
	*) ;;
esac
eval $cmd
