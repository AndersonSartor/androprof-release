package com.android.benchmark;

import android.app.Activity;
import android.os.Bundle;

public class Benchmark extends Activity{
	private static long time;
	private static int pid;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		time = System.currentTimeMillis();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		time = System.currentTimeMillis() - time;
		System.out.println("Benchmark finished;"+getApplicationContext().getPackageName()+";PID:"+pid+";Time(ms):"+time);
	}
	
	public static void setPid(int p){
		pid = p;
	}

}
