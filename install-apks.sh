#!/bin/bash

echo "Copy resourses? y or n"
read n

if [ $n = "y" ]; then
	./adb push ./benchmarks_SPEC/resources /sdcard/res/
fi

echo "Uninstalling old benchmarks..."
./adb uninstall com.android.spec.fft
./adb uninstall com.android.spec.fft.jni
./adb uninstall com.android.spec.lu
./adb uninstall com.android.spec.lu.jni
./adb uninstall com.android.spec.montecarlo
./adb uninstall com.android.spec.montecarlo.jni
./adb uninstall com.android.spec.sor
./adb uninstall com.android.spec.sor.jni
./adb uninstall com.android.spec.sparse
./adb uninstall com.android.spec.sparse.jni
./adb uninstall com.android.spec.serial
./adb uninstall com.android.spec.aes
./adb uninstall com.android.spec.rsa
./adb uninstall com.android.spec.signverify
./adb uninstall com.android.spec.compress
./adb uninstall com.android.spec.compress.jni
./adb uninstall com.android.spec.mpegaudio
./adb uninstall com.android.spec.mpegaudio.jni
./adb uninstall org.garret.bench

echo "Installing benchmarks...(Java)"
./adb install ./benchmarks_SPEC/java/crypto_aes_Spec.apk
./adb install ./benchmarks_SPEC/java/crypto_rsa_Spec.apk
./adb install ./benchmarks_SPEC/java/crypto_signverify_Spec.apk
./adb install ./benchmarks_SPEC/java/compress_Spec.apk
./adb install ./benchmarks_SPEC/java/mpegaudio_Spec.apk
./adb install ./benchmarks_SPEC/java/scimark_fft_Spec.apk
./adb install ./benchmarks_SPEC/java/scimark_lu_Spec.apk
./adb install ./benchmarks_SPEC/java/scimark_monteCarlo_Spec.apk
./adb install ./benchmarks_SPEC/java/scimark_sor_Spec.apk
./adb install ./benchmarks_SPEC/java/scimark_sparse_Spec.apk
./adb install ./benchmarks_SPEC/java/serial_Spec.apk
./adb install ./benchmarks_SPEC/java/sqlite_Spec.apk
echo "Installing benchmarks...(JNI)"
./adb install ./benchmarks_SPEC/JNI/compress_jni.apk
./adb install ./benchmarks_SPEC/JNI/mpegSpec_jni.apk
./adb install ./benchmarks_SPEC/JNI/scimark_fft_Spec_jni.apk
./adb install ./benchmarks_SPEC/JNI/scimark_lu_Spec_jni.apk
./adb install ./benchmarks_SPEC/JNI/scimark_montecarlo_jni.apk
./adb install ./benchmarks_SPEC/JNI/scimark_sor_Spec_jni.apk
./adb install ./benchmarks_SPEC/JNI/scimark_sparse_Spec_jni.apk
