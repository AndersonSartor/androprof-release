#!/bin/bash

export JAVA_HOME="/usr/lib64/jvm/java-1.5.0-gcj/bin"
. ./build/envsetup.sh
lunch sdk-eng
make -j8 clean
