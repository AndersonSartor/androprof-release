#!/bin/bash

source variables.sh

echo "Copy android 4.2 and 4.0.3 images? y or n"
read n

android4_2="backup-android-4.2"
android4_0_3="backup-android-4.0.3"

if [ $n = "y" ]; then
	mkdir -p $android4_2
	mkdir -p ./$android4_2/platforms
	mkdir -p ./$android4_2/system-images
	cp -r ./out/host/linux-x86/sdk/$user/platforms/android-17 ./$android4_2/platforms/android-17/
	cp -r ./out/host/linux-x86/sdk/$user/system-images/android-17 ./$android4_2/system-images/android-17/
	
	mkdir -p $android4_0_3
	mkdir -p ./$android4_0_3/platforms
	mkdir -p ./$android4_0_3/system-images
	cp -r ./out/host/linux-x86/sdk/$user/platforms/android-15 ./$android4_0_3/platforms/android-15/
	cp -r ./out/host/linux-x86/sdk/$user/system-images/android-15 ./$android4_0_3/system-images/android-15/
fi

. ./build/envsetup.sh
lunch sdk-eng
make -j4 sdk
cp -r ./external/qemu/pc-bios/keymaps ./out/host/linux-x86/sdk/$user/tools/
cp -r ./$android4_2/* ./out/host/linux-x86/sdk/$user/
cp -r ./$android4_0_3/* ./out/host/linux-x86/sdk/$user/
