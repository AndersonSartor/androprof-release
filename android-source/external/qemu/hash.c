#include <stdio.h>
#include "cache.h"
#include "khash.h"
#include "hash.h"

#define PID_N 20000

KHASH_MAP_INIT_INT64(64, struct entry*);
khash_t(64) *h[PID_N];
int created = 0;

void HashCreate(void)
{   
	int i;
	for (i = 0; i < PID_N; i++)
	{
        h[i] = kh_init(64);
    }
    created = 1;
}
void HashInsert(unsigned long pc, int pid, int bb_idx,unsigned long cache_count)
{
        if(!created)
            HashCreate();
        if(pid > PID_N)
        {
            perror("Pid exceeds 20000");
            return;
        }
        int ret;
        khiter_t k;
        k = kh_put(64,h[pid],pc,&ret);
        if(ret > 0)
        {
            struct entry *e = malloc(sizeof(struct entry));
            e->bb_idx = bb_idx;
            e->counter = cache_count;
            e->pc = pc;
            kh_value(h[pid], k) = e;
        } 
        else if (ret == 0) 
        {
            kh_value(h[pid],k)->counter += cache_count;     
        }
        else
        {
            perror("Error inserting in hash");
        }
}
void HashPrint(void)
{
	struct entry_t *s;
	FILE* arq = fopen("bb_info.txt", "w");
	fprintf(stderr, "Hits: %lu  -  Misses: %lu\n", hits, misses);
	int i;
    if(!created)
    {
        return;
    }
	for(i=0; i<CACHE_SIZE; i++){
		HashInsert(cache[i].pc, cache[i].pid, cache[i].bb, cache[i].counter);
	}
	int j;
        for(j = 0; j < PID_N;j++)
        {
                struct entry *s;
                kh_foreach_value(h[j],s,fprintf(arq, "0x%x,%d,%lu,%d\n", s->pc, j, s->counter, s->bb_idx));
        }
        fclose(arq);
        for(j = 0; j < PID_N;j++)
        {
                kh_destroy(64, h[j]);
        }
}

