cmd_arch/x86/boot/compressed/misc.o := gcc -Wp,-MD,arch/x86/boot/compressed/.misc.o.d  -nostdinc -isystem /usr/lib/gcc/x86_64-linux-gnu/4.4.3/include -Iinclude  -I/mnt/Anderson/android-kernel/goldfish/arch/x86/include -include include/linux/autoconf.h -D__KERNEL__ -m32 -D__KERNEL__ -O2 -fno-strict-aliasing -fPIC -ffreestanding -fno-stack-protector  -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(misc)"  -D"KBUILD_MODNAME=KBUILD_STR(misc)"  -c -o arch/x86/boot/compressed/misc.o arch/x86/boot/compressed/misc.c

deps_arch/x86/boot/compressed/misc.o := \
  arch/x86/boot/compressed/misc.c \
    $(wildcard include/config/paravirt.h) \
    $(wildcard include/config/x86/32.h) \
    $(wildcard include/config/x86/64.h) \
    $(wildcard include/config/x86/verbose/bootup.h) \
    $(wildcard include/config/relocatable.h) \
    $(wildcard include/config/physical/align.h) \
  include/linux/linkage.h \
  include/linux/compiler.h \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  include/linux/compiler-gcc4.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/linkage.h \
    $(wildcard include/config/x86/alignment/16.h) \
  include/linux/screen_info.h \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbd.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  include/linux/posix_types.h \
  include/linux/stddef.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/posix_types.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/posix_types_32.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/types.h \
    $(wildcard include/config/highmem64g.h) \
  include/asm-generic/int-ll64.h \
  include/linux/elf.h \
  include/linux/elf-em.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/elf.h \
    $(wildcard include/config/compat/vdso.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/ptrace.h \
    $(wildcard include/config/x86/debugctlmsr.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/ptrace-abi.h \
    $(wildcard include/config/x86/ptrace/bts.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/processor-flags.h \
    $(wildcard include/config/vm86.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/segment.h \
    $(wildcard include/config/smp.h) \
  include/linux/init.h \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/hotplug.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/user.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/user_32.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/page.h \
  include/linux/const.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/page_32.h \
    $(wildcard include/config/highmem4g.h) \
    $(wildcard include/config/page/offset.h) \
    $(wildcard include/config/4kstacks.h) \
    $(wildcard include/config/x86/pae.h) \
    $(wildcard include/config/hugetlb/page.h) \
    $(wildcard include/config/debug/virtual.h) \
    $(wildcard include/config/flatmem.h) \
    $(wildcard include/config/x86/use/3dnow.h) \
    $(wildcard include/config/x86/3dnow.h) \
  include/linux/string.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/string.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/string_32.h \
  include/asm-generic/pgtable-nopmd.h \
  include/asm-generic/pgtable-nopud.h \
  include/asm-generic/memory_model.h \
    $(wildcard include/config/discontigmem.h) \
    $(wildcard include/config/sparsemem/vmemmap.h) \
    $(wildcard include/config/sparsemem.h) \
  include/asm-generic/page.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/auxvec.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/vdso.h \
    $(wildcard include/config/compat.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/processor.h \
    $(wildcard include/config/x86/vsmp.h) \
    $(wildcard include/config/x86/ds.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/vm86.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/math_emu.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/sigcontext.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/current.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/percpu.h \
  include/asm-generic/percpu.h \
    $(wildcard include/config/debug/preempt.h) \
    $(wildcard include/config/have/setup/per/cpu/area.h) \
  include/linux/threads.h \
    $(wildcard include/config/nr/cpus.h) \
    $(wildcard include/config/base/small.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/cpufeature.h \
    $(wildcard include/config/x86/invlpg.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/required-features.h \
    $(wildcard include/config/x86/minimum/cpu/family.h) \
    $(wildcard include/config/math/emulation.h) \
    $(wildcard include/config/x86/cmpxchg64.h) \
    $(wildcard include/config/x86/cmov.h) \
    $(wildcard include/config/x86/p6/nop.h) \
  include/linux/bitops.h \
    $(wildcard include/config/generic/find/first/bit.h) \
    $(wildcard include/config/generic/find/last/bit.h) \
    $(wildcard include/config/generic/find/next/bit.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/bitops.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/alternative.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/asm.h \
  include/asm-generic/bitops/sched.h \
  include/asm-generic/bitops/hweight.h \
  include/asm-generic/bitops/fls64.h \
  include/asm-generic/bitops/ext2-non-atomic.h \
  include/asm-generic/bitops/le.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/byteorder.h \
  include/linux/byteorder/little_endian.h \
  include/linux/swab.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/swab.h \
    $(wildcard include/config/x86/bswap.h) \
  include/linux/byteorder/generic.h \
  include/asm-generic/bitops/minix.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/system.h \
    $(wildcard include/config/ia32/emulation.h) \
    $(wildcard include/config/x86/ppro/fence.h) \
    $(wildcard include/config/x86/oostore.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/cmpxchg.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/cmpxchg_32.h \
    $(wildcard include/config/x86/cmpxchg.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/nops.h \
    $(wildcard include/config/mk7.h) \
  include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/spinlock/sleep.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/printk/debug.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
  /usr/lib/gcc/x86_64-linux-gnu/4.4.3/include/stdarg.h \
  include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  include/linux/typecheck.h \
  include/linux/ratelimit.h \
  include/linux/param.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/param.h \
    $(wildcard include/config/hz.h) \
  include/linux/dynamic_printk.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/debug/bugverbose.h) \
  include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
    $(wildcard include/config/x86.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/irqflags.h \
    $(wildcard include/config/debug/lock/alloc.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/msr.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/msr-index.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/errno.h \
  include/asm-generic/errno.h \
  include/asm-generic/errno-base.h \
  include/linux/errno.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/desc_defs.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/ds.h \
  include/linux/err.h \
  include/linux/personality.h \
  include/linux/cpumask.h \
    $(wildcard include/config/disable/obsolete/cpumask/functions.h) \
    $(wildcard include/config/hotplug/cpu.h) \
    $(wildcard include/config/cpumask/offstack.h) \
    $(wildcard include/config/debug/per/cpu/maps.h) \
  include/linux/bitmap.h \
  include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/cache.h \
    $(wildcard include/config/x86/l1/cache/shift.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/desc.h \
  include/linux/io.h \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/has/ioport.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/io.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/io_32.h \
  include/asm-generic/iomap.h \
  include/linux/vmalloc.h \
  include/linux/spinlock.h \
    $(wildcard include/config/debug/spinlock.h) \
    $(wildcard include/config/generic/lockbreak.h) \
    $(wildcard include/config/preempt.h) \
  include/linux/preempt.h \
    $(wildcard include/config/preempt/notifiers.h) \
  include/linux/thread_info.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/thread_info.h \
    $(wildcard include/config/debug/stack/usage.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/ftrace.h \
    $(wildcard include/config/function/tracer.h) \
    $(wildcard include/config/dynamic/ftrace.h) \
    $(wildcard include/config/function/graph/tracer.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/atomic.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/atomic_32.h \
    $(wildcard include/config/m386.h) \
  include/asm-generic/atomic.h \
  include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  include/linux/poison.h \
  include/linux/prefetch.h \
  include/linux/stringify.h \
  include/linux/bottom_half.h \
  include/linux/spinlock_types.h \
  include/linux/spinlock_types_up.h \
  include/linux/lockdep.h \
    $(wildcard include/config/lockdep.h) \
    $(wildcard include/config/lock/stat.h) \
    $(wildcard include/config/generic/hardirqs.h) \
  include/linux/spinlock_up.h \
  include/linux/spinlock_api_up.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/boot.h \
    $(wildcard include/config/physical/start.h) \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/bootparam.h \
  include/linux/apm_bios.h \
  include/linux/ioctl.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/ioctl.h \
  include/asm-generic/ioctl.h \
  include/linux/edd.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/e820.h \
    $(wildcard include/config/nodes/shift.h) \
    $(wildcard include/config/efi.h) \
    $(wildcard include/config/hibernation.h) \
    $(wildcard include/config/memtest.h) \
  include/linux/ioport.h \
  /mnt/Anderson/android-kernel/goldfish/arch/x86/include/asm/ist.h \
  include/video/edid.h \
  arch/x86/boot/compressed/../../../../lib/inflate.c \

arch/x86/boot/compressed/misc.o: $(deps_arch/x86/boot/compressed/misc.o)

$(deps_arch/x86/boot/compressed/misc.o):
