#!/bin/bash

source variables.sh

export LD_LIBRARY_PATH="./out/host/linux-x86/sdk/$user/tools/lib"

echo "Choose the architecture:"
echo "1 - 4.2.2 ARM"
echo "2 - 4.2.2 x86"
echo "3 - 4.2.2 MIPS"
echo "4 - 4.0.3 ARM"
echo "5 - 4.0.3 x86"
echo "6 - 4.0.3 MIPS"
echo "Option:"
read n

emulator="./out/host/linux-x86/sdk/$user/tools/emulator"
avd=" -partition-size 1024 -avd "
options=" -trace MYTRACE -qemu -d in_asm"
x86=" -kernel ./goldfish/arch/i386/boot/bzImage"
x86_pid="./adb wait-for-device shell cat /proc/kmsg > x86_pids.txt"
cmd=""

case $n in
	1) cmd=$emulator$avd"android-ARM-4.2.2-master"$options;;
	2) cmd=$emulator$avd"android-x86-4.2.2-master"$x86$options" -disable-kvm & $x86_pid";;
	3) cmd=$emulator$avd"android-MIPS-4.2.2-master"$options;;
	4) cmd=$emulator$avd"android-ARM-4.0.3-master"$options;;
	5) cmd=$emulator$avd"android-x86-4.0.3-master"$x86$options" -disable-kvm & $x86_pid";;
	6) cmd=$emulator$avd"android-MIPS-4.0.3-master"$options;;
	*) ;;
esac
eval $cmd
