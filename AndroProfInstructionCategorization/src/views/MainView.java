package views;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.MainController;

public class MainView extends JFrame{

	private String[] columns = {"Instruction", "Type"};
	private static final long serialVersionUID = 1L;
	private JTextField tfDefaultCycles;
	private JTextField tfDefaultPower;
	private JTextField tfCategoryCycles;
	private JTextField tfCategoryPower;
	private JTable undefinedTable;
	private JTable categoryTable;
	private JTextField tfInstructionName;
	private JTextField tfCategory;
	private JMenuItem mntmImportBbLog;
	private JMenuItem mntmImportInstructionXmlFile;
	private JMenuItem mntmExportInstructionXml;
	private JComboBox comboBox;
	private JButton btnAddCategory;
	private JButton btnAddInstruction;
	private JButton btnAddToCategory;
	private JButton btnRemoveFromCategory;
	private JTextField tfInstructionType;
	private JButton btnDeleteSelectedCategory;
	private JButton btnUpdateDefault;
	private JButton btnUpdateCategory;
	private JTextField tfArch;
	private JButton btnSet;
	private JButton btnDeleteSelectedInstructions;

	public MainView() {
		setBounds(100, 100, 700, 700);
		setTitle("AndroProf: Instruction categorization");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("25dlu"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu:grow"),
				ColumnSpec.decode("10dlu"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		JPanel panel = new JPanel();
		getContentPane().add(panel, "2, 2, fill, fill");
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("85dlu"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("50dlu:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("65dlu"),}));

		JPanel panel_8 = new JPanel();
		panel.add(panel_8, "2, 2, fill, fill");
		panel_8.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				ColumnSpec.decode("5dlu"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("45dlu"),}));

		JLabel lblArchitectureName = new JLabel("Architecture name:");
		panel_8.add(lblArchitectureName, "2, 2, left, default");

		tfArch = new JTextField();
		panel_8.add(tfArch, "4, 2, fill, default");
		tfArch.setColumns(10);

		btnSet = new JButton("Set");
		panel_8.add(btnSet, "6, 2");

		JLabel lblAddInstruction = new JLabel("Add instruction:");
		panel_8.add(lblAddInstruction, "2, 4, left, default");

		JPanel panel_15 = new JPanel();
		panel_8.add(panel_15, "2, 6, 5, 1, fill, fill");
		panel_15.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JLabel lblName = new JLabel("Name:");
		panel_15.add(lblName, "2, 2, left, default");

		tfInstructionName = new JTextField();
		panel_15.add(tfInstructionName, "4, 2");
		tfInstructionName.setColumns(10);

		JLabel lblType = new JLabel("Type:");
		panel_15.add(lblType, "2, 4, left, default");

		tfInstructionType = new JTextField();
		panel_15.add(tfInstructionType, "4, 4, fill, default");
		tfInstructionType.setColumns(10);

		btnAddInstruction = new JButton("Add");
		panel_15.add(btnAddInstruction, "6, 4");

		JPanel panel_3 = new JPanel();
		panel.add(panel_3, "2, 4, fill, fill");
		panel_3.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		DefaultTableModel model = new DefaultTableModel(
				new Object [][]{},
				columns){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};

		JScrollPane scrollPane = new JScrollPane();
		panel_3.add(scrollPane, "2, 2, fill, fill");

		undefinedTable = new JTable();
		undefinedTable.setModel(model);

		TableRowSorter<DefaultTableModel> rowSorter = new TableRowSorter<DefaultTableModel>(model){
			@Override
			public void rowsInserted(int firstRow, int endRow) {
				try {
					super.rowsInserted(firstRow, endRow);
				} catch (IndexOutOfBoundsException ex) {
					//http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6582564
				}
			}

			@Override
			public int convertRowIndexToModel(int index) {
				try {
					return super.convertRowIndexToModel(index);
				} catch (NullPointerException ex) {
					//http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6582564
					return 0;
				}
			}
		};
		List<SortKey> sortKeys = new ArrayList<SortKey>();
		sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		rowSorter.setSortKeys(sortKeys);
		undefinedTable.setRowSorter(rowSorter);
		scrollPane.setViewportView(undefinedTable);

		JPanel panel_4 = new JPanel();
		panel.add(panel_4, "2, 6, fill, fill");
		panel_4.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JPanel panel_13 = new JPanel();
		panel_4.add(panel_13, "2, 2, 5, 1, left, fill");

		JLabel lblDefaultCosts = new JLabel("Default costs:");
		panel_13.add(lblDefaultCosts);

		JLabel lblDefaultCost = new JLabel("Cycles:");
		panel_4.add(lblDefaultCost, "4, 4, left, default");

		tfDefaultCycles = new JTextField();
		panel_4.add(tfDefaultCycles, "6, 4, fill, default");
		tfDefaultCycles.setColumns(10);

		JLabel lblDefaultPower = new JLabel("Power:");
		panel_4.add(lblDefaultPower, "4, 6, left, default");

		tfDefaultPower = new JTextField();
		panel_4.add(tfDefaultPower, "6, 6, fill, default");
		tfDefaultPower.setColumns(10);

		btnUpdateDefault = new JButton("Update");
		panel_4.add(btnUpdateDefault, "8, 6");

		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, "4, 2, center, fill");
		panel_2.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("1px"),
				ColumnSpec.decode("44px:grow"),},
				new RowSpec[] {
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("25px:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("40dlu"),
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("25px:grow"),}));

		JPanel panel_9 = new JPanel();
		panel_2.add(panel_9, "2, 2, fill, fill");

		JPanel panel_11 = new JPanel();
		panel_2.add(panel_11, "2, 4, center, fill");

		btnAddToCategory = new JButton(">");
		panel_11.add(btnAddToCategory);

		btnRemoveFromCategory = new JButton("<");
		panel_11.add(btnRemoveFromCategory);

		JPanel panel_10 = new JPanel();
		panel_2.add(panel_10, "2, 6, fill, fill");

		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, "6, 2, fill, fill");
		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("85dlu"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("50dlu:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("65dlu"),}));

		JPanel panel_5 = new JPanel();
		panel_1.add(panel_5, "1, 2, 2, 1, fill, fill");
		panel_5.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		JLabel lblAddCategory = new JLabel("Add category:");
		panel_5.add(lblAddCategory, "2, 2, left, default");

		tfCategory = new JTextField();
		panel_5.add(tfCategory, "4, 2, fill, default");
		tfCategory.setColumns(10);

		btnAddCategory = new JButton("Add");
		panel_5.add(btnAddCategory, "6, 2");

		JLabel lblSelectedCategory = new JLabel("Selected category:");
		panel_5.add(lblSelectedCategory, "2, 4, left, default");

		comboBox = new JComboBox();
		panel_5.add(comboBox, "4, 4, 3, 1, fill, default");

		JPanel panel_14 = new JPanel();
		panel_5.add(panel_14, "2, 6, 5, 1, fill, fill");
		panel_14.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("4px:grow"),
				ColumnSpec.decode("235px"),
				ColumnSpec.decode("4dlu:grow"),},
			new RowSpec[] {
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("25px"),
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("25px"),}));

		btnDeleteSelectedCategory = new JButton("Delete selected category");
		panel_14.add(btnDeleteSelectedCategory, "2, 2, fill, top");
		
		btnDeleteSelectedInstructions = new JButton("Delete selected instructions");
		panel_14.add(btnDeleteSelectedInstructions, "2, 4, fill, top");

		JPanel panel_6 = new JPanel();
		panel_1.add(panel_6, "1, 4, 2, 1, fill, fill");
		panel_6.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		DefaultTableModel modelCateg = new DefaultTableModel(
				new Object [][]{},
				columns){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};

		JScrollPane scrollPane_1 = new JScrollPane();
		panel_6.add(scrollPane_1, "2, 2, fill, fill");

		categoryTable = new JTable();
		categoryTable.setModel(modelCateg);

		TableRowSorter<DefaultTableModel> rowSorterCateg = new TableRowSorter<DefaultTableModel>(modelCateg){
			@Override
			public void rowsInserted(int firstRow, int endRow) {
				try {
					super.rowsInserted(firstRow, endRow);
				} catch (IndexOutOfBoundsException ex) {
					//http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6582564
				}
			}

			@Override
			public int convertRowIndexToModel(int index) {
				try {
					return super.convertRowIndexToModel(index);
				} catch (NullPointerException ex) {
					//http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6582564
					return 0;
				}
			}
		};
		List<SortKey> sortKeysCateg = new ArrayList<SortKey>();
		sortKeysCateg.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		rowSorterCateg.setSortKeys(sortKeysCateg);
		categoryTable.setRowSorter(rowSorterCateg);
		scrollPane_1.setViewportView(categoryTable);

		JPanel panel_7 = new JPanel();
		panel_1.add(panel_7, "2, 6, fill, fill");
		panel_7.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JPanel panel_12 = new JPanel();
		panel_7.add(panel_12, "2, 2, 5, 1, left, fill");

		JLabel lblSelectedCategoryCosts = new JLabel("Selected category costs:");
		panel_12.add(lblSelectedCategoryCosts);

		JLabel lblCost = new JLabel("Cycles:");
		panel_7.add(lblCost, "4, 4, left, default");

		tfCategoryCycles = new JTextField();
		panel_7.add(tfCategoryCycles, "6, 4, fill, default");
		tfCategoryCycles.setColumns(10);

		JLabel lblPower = new JLabel("Power:");
		panel_7.add(lblPower, "4, 6, right, default");

		tfCategoryPower = new JTextField();
		panel_7.add(tfCategoryPower, "6, 6, fill, default");
		tfCategoryPower.setColumns(10);

		btnUpdateCategory = new JButton("Update");
		panel_7.add(btnUpdateCategory, "8, 6");

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnImport = new JMenu("Import");
		menuBar.add(mnImport);

		mntmImportBbLog = new JMenuItem("Import BB log file");
		mnImport.add(mntmImportBbLog);

		mntmImportInstructionXmlFile = new JMenuItem("Import instruction XML file");
		mnImport.add(mntmImportInstructionXmlFile);

		JMenu mnExport = new JMenu("Export");
		menuBar.add(mnExport);

		mntmExportInstructionXml = new JMenuItem("Export instruction XML file");
		mnExport.add(mntmExportInstructionXml);

		setLocationRelativeTo(null);
		setVisible(true);
	}

	public void setController(MainController mc){
		this.mntmImportBbLog.addActionListener(mc);
		this.mntmImportInstructionXmlFile.addActionListener(mc);
		this.comboBox.addItemListener(mc);
		this.btnAddCategory.addActionListener(mc);
		this.btnAddInstruction.addActionListener(mc);
		this.btnAddToCategory.addActionListener(mc);
		this.btnRemoveFromCategory.addActionListener(mc);
		this.undefinedTable.getSelectionModel().addListSelectionListener(mc);
		this.categoryTable.getSelectionModel().addListSelectionListener(mc);
		this.btnDeleteSelectedCategory.addActionListener(mc);
		this.btnUpdateDefault.addActionListener(mc);
		this.btnUpdateCategory.addActionListener(mc);
		this.mntmExportInstructionXml.addActionListener(mc);
		this.btnSet.addActionListener(mc);
		this.btnDeleteSelectedInstructions.addActionListener(mc);
	}

	public JTextField getTfDefaultCycles() {
		return tfDefaultCycles;
	}

	public void setTfDefaultCycles(int defaultCycles) {
		this.tfDefaultCycles.setText(String.valueOf(defaultCycles));
	}

	public JTextField getTfDefaultPower() {
		return tfDefaultPower;
	}

	public void setTfDefaultPower(float defaultPower) {
		this.tfDefaultPower.setText(String.valueOf(defaultPower));
	}

	public JMenuItem getMntmImportBbLog() {
		return mntmImportBbLog;
	}

	public JMenuItem getMntmImportInstructionXmlFile() {
		return mntmImportInstructionXmlFile;
	}

	public JTable getUndefinedTable() {
		return undefinedTable;
	}

	public void setUndefinedTable(JTable undefinedTable) {
		this.undefinedTable = undefinedTable;
	}

	public JTable getCategoryTable() {
		return categoryTable;
	}

	public void setCategoryTable(JTable categoryTable) {
		this.categoryTable = categoryTable;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public JButton getBtnAddCategory() {
		return btnAddCategory;
	}

	public String getTfCategory() {
		return tfCategory.getText();
	}

	public void setTfCategory(String category) {
		this.tfCategory.setText(category);
	}

	public String getTfInstructionName() {
		return tfInstructionName.getText();
	}

	public void setTfInstructionName(String tfAddInstruction) {
		this.tfInstructionName.setText(tfAddInstruction);
	}

	public JButton getBtnAddInstruction() {
		return btnAddInstruction;
	}

	public JButton getBtnAddToCategory() {
		return btnAddToCategory;
	}

	public JButton getBtnRemoveFromCategory() {
		return btnRemoveFromCategory;
	}

	public String getTfInstructionType() {
		return tfInstructionType.getText();
	}

	public void setTfInstructionType(String tfInstructionType) {
		this.tfInstructionType.setText(tfInstructionType);
	}

	public JButton getBtnDeleteSelectedCategory() {
		return btnDeleteSelectedCategory;
	}

	public JButton getBtnUpdateDefault() {
		return btnUpdateDefault;
	}

	public JButton getBtnUpdateCategory() {
		return btnUpdateCategory;
	}

	public String getTfCategoryCycles() {
		return tfCategoryCycles.getText();
	}

	public void setTfCategoryCycles(int tfCategoryCycles) {
		this.tfCategoryCycles.setText(String.valueOf(tfCategoryCycles));
	}

	public String getTfCategoryPower() {
		return tfCategoryPower.getText();
	}

	public void setTfCategoryPower(float tfCategoryPower) {
		this.tfCategoryPower.setText(String.valueOf(tfCategoryPower));
	}

	public JMenuItem getMntmExportInstructionXml() {
		return mntmExportInstructionXml;
	}

	public JTextField getTfArch() {
		return tfArch;
	}

	public void setTfArch(String tfArch) {
		this.tfArch.setText(tfArch);
	}

	public JButton getBtnSet() {
		return btnSet;
	}

	public JButton getBtnDeleteSelectedInstructions() {
		return btnDeleteSelectedInstructions;
	}

}
