package views;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.ImportBbLogController;

public class ImportBbLogView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnBrowseBbLogFIle;
	private JPanel panel_1;
	private JPanel panel_3;
	private JButton btnOk;
	private JButton btnCancel;
	public JProgressBar progressBar;
	private JPanel panel;
	private JLabel lblArchitectureName;
	private JTextField tfArch;
	private JTextField tfBbLogFilePath;


	/**
	 * Create the frame.
	 */
	public ImportBbLogView() {
		setBounds(100, 100, 400, 292);
		setTitle("Import BB file");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.PARAGRAPH_GAP_ROWSPEC,
				FormFactory.PREF_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("40px"),
				RowSpec.decode("10dlu"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		panel = new JPanel();
		contentPane.add(panel, "2, 2, left, fill");
		
		lblArchitectureName = new JLabel("Architecture name:");
		panel.add(lblArchitectureName);
		
		tfArch = new JTextField();
		contentPane.add(tfArch, "2, 4, fill, default");
		tfArch.setColumns(10);
		
		panel_1 = new JPanel();
		
		JLabel lblBbLogFile = new JLabel("BB log File:");
		panel_1.add(lblBbLogFile);
		contentPane.add(panel_1, "2, 6, left, fill");
		
		tfBbLogFilePath = new JTextField();
		contentPane.add(tfBbLogFilePath, "2, 7, fill, default");
		tfBbLogFilePath.setColumns(10);
		
		btnBrowseBbLogFIle = new JButton("Browse...");
		contentPane.add(btnBrowseBbLogFIle, "3, 7");
		
		progressBar = new JProgressBar();
		contentPane.add(progressBar, "2, 9, 2, 2");
		
		panel_3 = new JPanel();
		contentPane.add(panel_3, "2, 13, 2, 1, fill, fill");
		
		btnOk = new JButton("OK");
		panel_3.add(btnOk);
		
		btnCancel = new JButton("Cancel");
		panel_3.add(btnCancel);
		
		setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	public void setController(ImportBbLogController lc){
		this.btnOk.addActionListener(lc);
		this.btnCancel.addActionListener(lc);
		this.btnBrowseBbLogFIle.addActionListener(lc);
		
	}

	public JButton getBtnBrowseBbLogFile() {
		return btnBrowseBbLogFIle;
	}

	public JButton getBtnOk() {
		return btnOk;
	}

	public JButton getBtnCancel() {
		return btnCancel;
	}

	public JTextField getTfArch() {
		return tfArch;
	}

	public JTextField getTfBbLogFilePath() {
		return tfBbLogFilePath;
	}

	public void setTfArch(JTextField tfArch) {
		this.tfArch = tfArch;
	}

	public void setTfBbLogFilePath(String bbLogFilePath) {
		this.tfBbLogFilePath.setText(bbLogFilePath);
	}

}
