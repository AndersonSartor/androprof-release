package instrXmlBuilder;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import views.MainView;
import controllers.MainController;

public class Main {

	public static void main(String[] args) {
		try{
			for(LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()){
				if("Nimbus".equals(info.getName())){
					UIManager.setLookAndFeel(info.getClassName());
				}
			}
		}
		catch(Exception e){
		}
		
		MainView mainView = new MainView();
		MainController mainController = new MainController(mainView);
		mainView.setController(mainController);

	}

}
