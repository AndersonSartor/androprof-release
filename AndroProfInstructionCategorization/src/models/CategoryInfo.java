package models;

import java.util.ArrayList;
import java.util.List;

public class CategoryInfo {
	
	private int cycles;
	private float power;
	private List<String> instructionAndTypeList;
	
	public CategoryInfo(){
		instructionAndTypeList = new ArrayList<String>();
	}

	public int getCycles() {
		return cycles;
	}

	public void setCycles(int cycles) {
		this.cycles = cycles;
	}

	public float getPower() {
		return power;
	}

	public void setPower(float power) {
		this.power = power;
	}

	public List<String> getInstructionAndTypeList() {
		return instructionAndTypeList;
	}

	public void setInstructionAndTypeList(List<String> instructionAndTypeList) {
		this.instructionAndTypeList = instructionAndTypeList;
	}


}
