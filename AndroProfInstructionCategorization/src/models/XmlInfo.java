package models;

import java.io.File;
import java.util.HashMap;

public class XmlInfo {
	
	private File xmlFile;
	private File bbFile;
	private String arch = "";
	private HashMap<String, CategoryInfo> categoryHashMap;
	
	public XmlInfo(){
		categoryHashMap = new HashMap<String, CategoryInfo>();
	}
	
	public File getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	public File getBbFile() {
		return bbFile;
	}
	public void setBbFile(File bbFile) {
		this.bbFile = bbFile;
	}

	public HashMap<String, CategoryInfo> getCategoryHashMap() {
		return categoryHashMap;
	}

	public void setCategoryHashMap(HashMap<String, CategoryInfo> instrHashMap) {
		this.categoryHashMap = instrHashMap;
	}

	public String getArch() {
		return arch;
	}

	public void setArch(String arch) {
		this.arch = arch;
	}

}
