package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;

import models.CategoryInfo;
import models.XmlInfo;
import views.ErrorDialogView;
import views.ImportBbLogView;

public class ImportBbLogController implements ActionListener{

	private ImportBbLogView importBbLogView;
	private final JFileChooser fc = new JFileChooser();
	private XmlInfo xmlInfo;
	private MainController mainController;
	public static enum Arch {ARM, x86, MIPS};

	public ImportBbLogController(ImportBbLogView importBbLogView, MainController mainController){
		this.importBbLogView = importBbLogView;
		this.mainController = mainController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == importBbLogView.getBtnBrowseBbLogFile()){
			int returnVal = fc.showOpenDialog(importBbLogView);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				xmlInfo = new XmlInfo();
				xmlInfo.setXmlFile(file);
				importBbLogView.setTfBbLogFilePath(file.toString());
			} 
		}

		if(e.getSource() == importBbLogView.getBtnOk()){
			if(!importBbLogView.getTfArch().getText().isEmpty())
				if(!importBbLogView.getTfBbLogFilePath().getText().isEmpty())
					new Thread(new refreshThread()).start();
				else
					new ErrorDialogView("The BB log file path must be specified");
			else
				new ErrorDialogView("The architecture must be specified");
		}

		if(e.getSource() == importBbLogView.getBtnCancel()){
			importBbLogView.dispose();
		}

	}

	public class refreshThread implements Runnable{

		@Override
		public void run() {
			importBbLogView.progressBar.setIndeterminate(true);
			importBbLog();
			mainController.setXmlInfo(xmlInfo);
			mainController.refresh();
			importBbLogView.dispose();
		}

	}

	private void importBbLog(){
		xmlInfo = new XmlInfo();
		xmlInfo.setArch(importBbLogView.getTfArch().getText());
		FileInputStream logFile = null;
		List<String> allInstructions = mainController.getAllInstructions();
		try {
			logFile = new FileInputStream(importBbLogView.getTfBbLogFilePath().getText());
			Scanner scanner = new Scanner(logFile);

			HashMap<String, CategoryInfo> categoryHashMap = new HashMap<String, CategoryInfo>();
			List<String> instructionAndTypeList = new ArrayList<String>();
			Arch arch = null;
			if(xmlInfo.getArch().toLowerCase().startsWith("arm"))
				arch = Arch.ARM;
			else if(xmlInfo.getArch().toLowerCase().startsWith("x86"))
				arch = Arch.x86;
			else if(xmlInfo.getArch().toLowerCase().startsWith("mips"))
				arch = Arch.MIPS;
			while(scanner.hasNextLine()){
				String currentLine = scanner.nextLine();
				if(currentLine.equals("----------------")){
					scanner.nextLine();
					String bbLine= "";
					do
					{
						bbLine = scanner.nextLine();
						if(!bbLine.isEmpty()){
							String[] instrLine = bbLine.split("\\s+");
							String instr = "";
							String type = "Undefined";

							if(arch == Arch.ARM){
								if(instrLine[1].length()==8){
									//code = instrLine[1];
									instr = instrLine[2];
									type = "ARM";
								}
								else if(instrLine[1].length()==4){
									String substr = bbLine.substring(18);
									if(substr.indexOf(instrLine[2]) == 0){
										//code = instrLine[1] + " " + instrLine[2];
										instr = instrLine[3];
									}
									else{
										//code = instrLine[1];
										instr = instrLine[2];
									}
									type = "Thumb";
								}
							}
							else{
								instr = instrLine[1];
								if(arch == Arch.x86)
									type = "x86";
								else if(arch == Arch.MIPS)
									type = "MIPS";
							}
							String instructionAndType = instr+";"+type;
							if(!allInstructions.contains(instructionAndType)){
								instructionAndTypeList.add(instructionAndType);
								allInstructions.add(instructionAndType);
							}
						}
					}
					while(!bbLine.isEmpty());
					CategoryInfo categoryInfo = new CategoryInfo();
					categoryInfo.setCycles(mainController.getDefaultCycles());
					categoryInfo.setPower(mainController.getDefaultPower());
					categoryInfo.setInstructionAndTypeList(instructionAndTypeList);
					categoryHashMap.put("Undefined", categoryInfo);
					xmlInfo.setCategoryHashMap(categoryHashMap);
					mainController.setAllInstructions(allInstructions);
					mainController.setXmlInfo(xmlInfo);
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			new ErrorDialogView("File not found");
		}
	}

}

