package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import models.CategoryInfo;
import models.XmlInfo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import views.ErrorDialogView;
import views.ImportBbLogView;
import views.ImportInstrXmlView;
import views.Loading;
import views.MainView;

public class MainController implements ActionListener,ItemListener,ListSelectionListener {

	private MainView mainView;
	private MainController context;
	private XmlInfo xmlInfo;
	private int defaultCycles = 1;
	private float defaultPower = 2;
	private Thread thread;
	private Thread exportThread;
	private List<String> selectedUndefinedInstructions;
	private List<String> selectedCategoryInstructions;
	private List<String> allInstructions;
	private String previousCategory = "";
	private File exportedXmlFile = null;

	public MainController(MainView mainView) {
		this.mainView = mainView;
		this.context = this;
		selectedUndefinedInstructions = new ArrayList<String>();
		selectedCategoryInstructions = new ArrayList<String>();
		allInstructions = new ArrayList<String>();
		xmlInfo = new XmlInfo();
		mainView.setTfDefaultCycles(defaultCycles);
		mainView.setTfDefaultPower(defaultPower);

		CategoryInfo categoryInfo = new CategoryInfo();
		categoryInfo.setCycles(defaultCycles);
		categoryInfo.setPower(defaultPower);
		categoryInfo.setInstructionAndTypeList(new ArrayList<String>());
		HashMap<String, CategoryInfo> categoryHashMap = new HashMap<String, CategoryInfo>();
		categoryHashMap.put("Undefined", categoryInfo);
		xmlInfo.setCategoryHashMap(categoryHashMap);
	}

	public void refresh() {
		thread = new Thread(new refreshThread());
		thread.start();
	}

	public class refreshThread implements Runnable {

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			refreshWindow();
			loading.dispose();
		}

	}

	public class exportThread implements Runnable {

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			exportXml();
			loading.dispose();
			ErrorDialogView saved = new ErrorDialogView("File saved!");
			saved.setTitle("Saved");
		}

	}

	public void refreshWindow() {
		mainView.setTfArch(xmlInfo.getArch());
		mainView.getComboBox().removeAllItems();
		HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
		Set<Entry<String, CategoryInfo>> p = categoryHashMap.entrySet();
		Iterator<Entry<String, CategoryInfo>> categIt = p.iterator();
		while (categIt.hasNext()) {
			Map.Entry<String, CategoryInfo> categoryEntry = (Map.Entry<String, CategoryInfo>) categIt.next();
			if (categoryEntry.getKey().equals("Undefined")) {
				defaultCycles = categoryEntry.getValue().getCycles();
				defaultPower = categoryEntry.getValue().getPower();
				mainView.setTfDefaultCycles(defaultCycles);
				mainView.setTfDefaultPower(defaultPower);
				DefaultTableModel dtm = (DefaultTableModel) mainView.getUndefinedTable().getModel();
				dtm.setRowCount(0);
				for (String instruction : categoryEntry.getValue().getInstructionAndTypeList()) {
					String[] values = instruction.split(";");
					dtm.addRow(new Object[] { values[0], values[1] });
				}
			} 
			else {
				mainView.getComboBox().addItem(categoryEntry.getKey());
			}
		}
		if(!previousCategory.isEmpty()){
			try{
				mainView.getComboBox().setSelectedItem(previousCategory);
			}
			catch(Exception e){
			}
		}
	}

	private void addCategory(){
		String category = mainView.getTfCategory().trim();
		HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
		if(categoryHashMap.get(category) == null){
			CategoryInfo categoryInfo = new CategoryInfo();
			categoryInfo.setCycles(defaultCycles);
			categoryInfo.setPower(defaultPower);
			categoryInfo.setInstructionAndTypeList(new ArrayList<String>());
			categoryHashMap.put(category, categoryInfo);
			xmlInfo.setCategoryHashMap(categoryHashMap);
			mainView.setTfCategory("");
			previousCategory = category;
			refresh();
		}
		else{
			mainView.setTfCategory("");
			new ErrorDialogView("Category "+category+" already exists");
		}
	}

	private void addInstruction(){
		String instructionName = mainView.getTfInstructionName();
		HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
		CategoryInfo categoryInfo = categoryHashMap.get("Undefined");
		List<String> instructionList = categoryInfo.getInstructionAndTypeList();
		String instruction = instructionName.trim()+";"+mainView.getTfInstructionType().trim();
		if(!allInstructions.contains(instruction)){
			allInstructions.add(instruction);
			instructionList.add(instruction);
			categoryInfo.setInstructionAndTypeList(instructionList);
			categoryHashMap.put("Undefined", categoryInfo);
		}
		else
			new ErrorDialogView("Instruction "+instruction+" already exists");
		mainView.setTfInstructionName("");
		mainView.setTfInstructionType("");
		refresh();
	}

	private void addInstructionToCategory(){
		if(mainView.getComboBox().getSelectedItem()!=null){
			String selectedCategory = mainView.getComboBox().getSelectedItem().toString();
			HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
			CategoryInfo categoryInfoUndefined = categoryHashMap.get("Undefined");
			CategoryInfo categoryInfoCategory = categoryHashMap.get(selectedCategory);
			List<String> instructionsUndefined = categoryInfoUndefined.getInstructionAndTypeList();
			List<String> instructionsCategory = categoryInfoCategory.getInstructionAndTypeList();
			for(String instruction : selectedUndefinedInstructions){
				instructionsUndefined.remove(instruction);
				instructionsCategory.add(instruction);
			}
			categoryInfoUndefined.setInstructionAndTypeList(instructionsUndefined);
			categoryInfoCategory.setInstructionAndTypeList(instructionsCategory);
			categoryHashMap.put("Undefined", categoryInfoUndefined);
			categoryHashMap.put(selectedCategory, categoryInfoCategory);
			previousCategory = selectedCategory;
			refresh();
		}
		else
			new ErrorDialogView("No category is selected");
	}

	private void removeInstructionFromCategory(){
		if(mainView.getComboBox().getSelectedItem()!=null){
			String selectedCategory = mainView.getComboBox().getSelectedItem().toString();
			HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
			CategoryInfo categoryInfoUndefined = categoryHashMap.get("Undefined");
			CategoryInfo categoryInfoCategory = categoryHashMap.get(selectedCategory);
			List<String> instructionsUndefined = categoryInfoUndefined.getInstructionAndTypeList();
			List<String> instructionsCategory = categoryInfoCategory.getInstructionAndTypeList();
			for(String instruction : selectedCategoryInstructions){
				instructionsCategory.remove(instruction);
				instructionsUndefined.add(instruction);
			}
			categoryInfoUndefined.setInstructionAndTypeList(instructionsUndefined);
			categoryInfoCategory.setInstructionAndTypeList(instructionsCategory);
			categoryHashMap.put("Undefined", categoryInfoUndefined);
			categoryHashMap.put(selectedCategory, categoryInfoCategory);
			previousCategory = selectedCategory;
			refresh();
		}
		else
			new ErrorDialogView("No category is selected");
	}
	
	private void deleteSelectedInstructions(){
		if(mainView.getComboBox().getSelectedItem()!=null){
			String selectedCategory = mainView.getComboBox().getSelectedItem().toString();
			HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
			CategoryInfo categoryInfoCategory = categoryHashMap.get(selectedCategory);
			List<String> instructionsCategory = categoryInfoCategory.getInstructionAndTypeList();
			for(String instruction : selectedCategoryInstructions){
				instructionsCategory.remove(instruction);
				allInstructions.remove(instruction);
			}
			categoryInfoCategory.setInstructionAndTypeList(instructionsCategory);
			categoryHashMap.put(selectedCategory, categoryInfoCategory);
		}
		HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
		CategoryInfo categoryInfoUndefined = categoryHashMap.get("Undefined");
		List<String> instructionsUndefined = categoryInfoUndefined.getInstructionAndTypeList();
		for(String instruction : selectedUndefinedInstructions){
			instructionsUndefined.remove(instruction);
			allInstructions.remove(instruction);
		}
		categoryInfoUndefined.setInstructionAndTypeList(instructionsUndefined);
		categoryHashMap.put("Undefined", categoryInfoUndefined);
		refresh();
		
	}

	private void deleteSelectedCategory(){
		if(mainView.getComboBox().getSelectedItem()!=null){
			previousCategory = "";
			HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
			CategoryInfo categoryInfoUndefined = categoryHashMap.get("Undefined");
			CategoryInfo categoryInfoCategory = categoryHashMap.get(mainView.getComboBox().getSelectedItem().toString());
			List<String> instructionsUndefined = categoryInfoUndefined.getInstructionAndTypeList();
			List<String> instructionsCategory = categoryInfoCategory.getInstructionAndTypeList();
			Iterator<String> catIt = instructionsCategory.iterator();
			while(catIt.hasNext()){
				String instruction = catIt.next();
				catIt.remove();
				instructionsUndefined.add(instruction);
			}
			categoryInfoUndefined.setInstructionAndTypeList(instructionsUndefined);
			categoryHashMap.put("Undefined", categoryInfoUndefined);
			categoryHashMap.remove(mainView.getComboBox().getSelectedItem().toString());
			DefaultTableModel dtm = (DefaultTableModel) mainView.getCategoryTable().getModel();
			dtm.setRowCount(0);
			refresh();
		}
		else
			new ErrorDialogView("No category is selected");
	}

	private void updateDefaultCosts(){
		if(!mainView.getTfDefaultCycles().getText().isEmpty() && !mainView.getTfDefaultPower().getText().isEmpty()){
			try{
				defaultCycles = Integer.parseInt(mainView.getTfDefaultCycles().getText());
				defaultPower = Float.parseFloat(mainView.getTfDefaultPower().getText());
				HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
				CategoryInfo categoryInfo = categoryHashMap.get("Undefined");
				categoryInfo.setCycles(defaultCycles);
				categoryInfo.setPower(defaultPower);
				categoryHashMap.put("Undefined", categoryInfo);
				mainView.setTfDefaultCycles(defaultCycles);
				mainView.setTfDefaultPower(defaultPower);

			}
			catch(NumberFormatException e ){
				new ErrorDialogView("There are invalid costs");
			}
		}
		else
			new ErrorDialogView("There are empty costs");
	}

	private void updateCategoryCosts(){
		if(mainView.getComboBox().getSelectedItem() != null){
			if(!mainView.getTfCategoryCycles().isEmpty() && !mainView.getTfCategoryPower().isEmpty()){
				try{
					int cycles = Integer.parseInt(mainView.getTfCategoryCycles());
					float power = Float.parseFloat(mainView.getTfCategoryPower());
					HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
					String category = mainView.getComboBox().getSelectedItem().toString();
					CategoryInfo categoryInfo = categoryHashMap.get(category);
					categoryInfo.setCycles(cycles);
					categoryInfo.setPower(power);
					categoryHashMap.put(category, categoryInfo);
					mainView.setTfCategoryCycles(cycles);
					mainView.setTfCategoryPower(power);
				}
				catch(NumberFormatException e ){
					new ErrorDialogView("There are invalid costs");
				}
			}
			else
				new ErrorDialogView("There are empty costs");
		}
	}

	private void exportXml(){
		try {
			HashMap<String, CategoryInfo> hashMap = xmlInfo.getCategoryHashMap();
			Set<Entry<String, CategoryInfo>> p=hashMap.entrySet();
			Iterator<Entry<String, CategoryInfo>> categIt=p.iterator();

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			docBuilder = docFactory.newDocumentBuilder();


			// root element
			Document architectures = docBuilder.newDocument();
			Element rootElement = architectures.createElement("architectures");
			architectures.appendChild(rootElement);

			//architecture
			Element arch = architectures.createElement("arch");
			rootElement.appendChild(arch);
			arch.setAttribute("name", xmlInfo.getArch());

			while(categIt.hasNext())
			{
				Map.Entry<String, CategoryInfo> ins =(Map.Entry<String, CategoryInfo>)categIt.next();
				String category=(String)ins.getKey();
				CategoryInfo categoryInfo = (CategoryInfo)ins.getValue();

				//categories
				Element categElem = architectures.createElement("category");
				arch.appendChild(categElem);
				categElem.setAttribute("name", category);
				categElem.setAttribute("cycles", String.valueOf(categoryInfo.getCycles()));
				categElem.setAttribute("power", String.valueOf(categoryInfo.getPower()));

				//instructions
				for(String instruction : categoryInfo.getInstructionAndTypeList()){
					Element instructionEle = architectures.createElement("instruction");
					categElem.appendChild(instructionEle);
					String instrv[] = instruction.split(";");
					instructionEle.setAttribute("name", instrv[0]);
					instructionEle.setAttribute("type", instrv[1]);
				}

			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(architectures);
			StreamResult result = new StreamResult(exportedXmlFile);
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mainView.getMntmImportBbLog()) {
			ImportBbLogView importBbLogView = new ImportBbLogView();
			ImportBbLogController importBbLogController = new ImportBbLogController(
					importBbLogView, context);
			importBbLogView.setController(importBbLogController);
		}
		if (e.getSource() == mainView.getMntmImportInstructionXmlFile()) {
			ImportInstrXmlView importInstrXmlView = new ImportInstrXmlView();
			ImportInstrXmlController importInstrXmlController = new ImportInstrXmlController(
					importInstrXmlView, context);
			importInstrXmlView.setController(importInstrXmlController);
		}
		if(e.getSource() == mainView.getBtnAddCategory()){
			if(!mainView.getTfCategory().isEmpty()){
				addCategory();
			}
		}
		if(e.getSource() == mainView.getBtnAddInstruction()){
			if(!mainView.getTfInstructionName().isEmpty() && !mainView.getTfInstructionType().isEmpty()){
				addInstruction();
			}
			else
				new ErrorDialogView("Instruction name and type must be specified");
		}
		if(e.getSource() == mainView.getBtnAddToCategory()){
			addInstructionToCategory();
		}
		if(e.getSource() == mainView.getBtnRemoveFromCategory()){
			removeInstructionFromCategory();
		}
		if(e.getSource() == mainView.getBtnDeleteSelectedInstructions()){
			deleteSelectedInstructions();
		}
		if(e.getSource() == mainView.getBtnDeleteSelectedCategory()){
			deleteSelectedCategory();
		}
		if(e.getSource() == mainView.getBtnUpdateDefault()){
			updateDefaultCosts();
		}
		if(e.getSource() == mainView.getBtnUpdateCategory()){
			updateCategoryCosts();
		}
		if(e.getSource() == mainView.getMntmExportInstructionXml()){
			if(!xmlInfo.getArch().isEmpty()){
				JFileChooser fc = new JFileChooser();
				int returnVal = fc.showSaveDialog(mainView);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					exportedXmlFile = fc.getSelectedFile();
					exportThread = new Thread(new exportThread());
					exportThread.start();
				}				
			}
			else
				new ErrorDialogView("Architecture name is not specified");
		}
		if(e.getSource() == mainView.getBtnSet()){
			xmlInfo.setArch(mainView.getTfArch().getText());
		}

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getStateChange() == ItemEvent.SELECTED){
			if(mainView.getComboBox().getSelectedItem() != null){
				HashMap<String, CategoryInfo> categoryHashMap = xmlInfo.getCategoryHashMap();
				DefaultTableModel dtm = (DefaultTableModel) mainView.getCategoryTable().getModel();
				dtm.setRowCount(0);
				for(String instruction : categoryHashMap.get(mainView.getComboBox().getSelectedItem()).getInstructionAndTypeList()){
					String[] values = instruction.split(";");
					dtm.addRow(new Object[] { values[0], values[1] });
				}
				String category = mainView.getComboBox().getSelectedItem().toString();
				CategoryInfo categoryInfo = categoryHashMap.get(category);
				mainView.setTfCategoryCycles(categoryInfo.getCycles());
				mainView.setTfCategoryPower(categoryInfo.getPower());
			}
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(e.getSource() == mainView.getUndefinedTable().getSelectionModel()){
			selectedUndefinedInstructions = new ArrayList<String>();
			JTable undefinedTable = mainView.getUndefinedTable();
			int selectionMode = undefinedTable.getSelectionModel().getSelectionMode();
			if (selectionMode == ListSelectionModel.SINGLE_SELECTION) {
				int rowIndex = undefinedTable.getSelectedRow();
				selectedUndefinedInstructions.add(undefinedTable.getValueAt(rowIndex, 0)+";"+
						undefinedTable.getValueAt(rowIndex, 1));
			} 
			else if (selectionMode == ListSelectionModel.SINGLE_INTERVAL_SELECTION || 
					selectionMode == ListSelectionModel.MULTIPLE_INTERVAL_SELECTION) {
				int rowIndexStart = undefinedTable.getSelectedRow();
				int rowIndexEnd = undefinedTable.getSelectionModel().getMaxSelectionIndex();
				for (int i = rowIndexStart; i <= rowIndexEnd; i++) {
					if (undefinedTable.isCellSelected(i, 0) || undefinedTable.isCellSelected(i, 1)) {
						selectedUndefinedInstructions.add(undefinedTable.getValueAt(i, 0)+";"+
								undefinedTable.getValueAt(i, 1));
					}
				}
			}
		}
		if(e.getSource() == mainView.getCategoryTable().getSelectionModel()){
			selectedCategoryInstructions = new ArrayList<String>();
			JTable categoryTable = mainView.getCategoryTable();
			int selectionMode = categoryTable.getSelectionModel().getSelectionMode();
			if (selectionMode == ListSelectionModel.SINGLE_SELECTION) {
				int rowIndex = categoryTable.getSelectedRow();
				selectedCategoryInstructions.add(categoryTable.getValueAt(rowIndex, 0)+";"+
						categoryTable.getValueAt(rowIndex, 1));
			} 
			else if (selectionMode == ListSelectionModel.SINGLE_INTERVAL_SELECTION || 
					selectionMode == ListSelectionModel.MULTIPLE_INTERVAL_SELECTION) {
				int rowIndexStart = categoryTable.getSelectedRow();
				int rowIndexEnd = categoryTable.getSelectionModel().getMaxSelectionIndex();
				for (int i = rowIndexStart; i <= rowIndexEnd; i++) {
					if (categoryTable.isCellSelected(i, 0) || categoryTable.isCellSelected(i, 1)) {
						selectedCategoryInstructions.add(categoryTable.getValueAt(i, 0)+";"+
								categoryTable.getValueAt(i, 1));
					}
				}
			}
		}
	}

	public XmlInfo getXmlInfo() {
		return xmlInfo;
	}

	public void setXmlInfo(XmlInfo xmlInfo) {
		this.xmlInfo = xmlInfo;
	}

	public int getDefaultCycles() {
		return defaultCycles;
	}

	public float getDefaultPower() {
		return defaultPower;
	}

	public List<String> getAllInstructions() {
		return allInstructions;
	}

	public void setAllInstructions(List<String> allInstructions) {
		this.allInstructions = allInstructions;
	}

}
