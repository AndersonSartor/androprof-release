package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import models.CategoryInfo;
import models.XmlInfo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import views.ErrorDialogView;
import views.ImportInstrXmlView;

public class ImportInstrXmlController implements ActionListener{

	private ImportInstrXmlView importInstrXmlView;
	private final JFileChooser fc = new JFileChooser();
	private XmlInfo xmlInfo;
	private MainController mainController;

	public ImportInstrXmlController(ImportInstrXmlView importInstrXmlView, MainController mainController){
		this.importInstrXmlView = importInstrXmlView;
		this.mainController = mainController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == importInstrXmlView.getBtnBrowseXmlFIle()){
			int returnVal = fc.showOpenDialog(importInstrXmlView);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				xmlInfo = new XmlInfo();
				xmlInfo.setXmlFile(file);
				importInstrXmlView.getXmlFilePath().setText(file.toString());
			} 
		}

		if(e.getSource() == importInstrXmlView.getBtnOk()){
			if(!importInstrXmlView.getXmlFilePath().getText().isEmpty())
				new Thread(new refreshThread()).start();
			else
				new ErrorDialogView("Instruction XML file path must be specified");
		}

		if(e.getSource() == importInstrXmlView.getBtnCancel()){
			importInstrXmlView.dispose();
		}

	}

	public class refreshThread implements Runnable{

		@Override
		public void run() {
			importInstrXmlView.progressBar.setIndeterminate(true);
			importXml();
			mainController.setXmlInfo(xmlInfo);
			importInstrXmlView.dispose();
			mainController.refresh();
		}

	}

	private void importXml(){
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlInfo.getXmlFile());
			doc.getDocumentElement().normalize();

			NodeList archList = doc.getElementsByTagName("arch");
			HashMap<String, CategoryInfo> categoryHashMap = new HashMap<String, CategoryInfo>();

			List<String> allInstructions = mainController.getAllInstructions();
			for (int currentArchIndex = 0; currentArchIndex < archList.getLength(); currentArchIndex++) {
				Node currentArch = archList.item(currentArchIndex);
				NodeList categoryList = currentArch.getChildNodes();

				Element archEle = (Element) currentArch;
				String archName = archEle.getAttribute("name");
				xmlInfo.setArch(archName);

				for (int currentCategoryIndex = 0; currentCategoryIndex < categoryList.getLength(); currentCategoryIndex++){
					Node currentCategory = categoryList.item(currentCategoryIndex);
					NodeList instructionList = currentCategory.getChildNodes();

					Element categoryEle = (Element) currentCategory;
					String categoryName = categoryEle.getAttribute("name");

					CategoryInfo categoryInfo = new CategoryInfo();
					int cycles;
					try{
						cycles = Integer.parseInt(categoryEle.getAttribute("cycles"));
					}
					catch(NumberFormatException e ){
						cycles = 0;
					}
					categoryInfo.setCycles(cycles);

					float power;
					try{
						power = Float.parseFloat(categoryEle.getAttribute("power"));
					}
					catch(NumberFormatException e ){
						power = 0;
					}
					categoryInfo.setPower(power);

					List<String> instructionAndTypeList = new ArrayList<String>();
					for(int currentInstructionIndex = 0; currentInstructionIndex < instructionList.getLength(); currentInstructionIndex++){
						Node currentInstruction = instructionList.item(currentInstructionIndex);
						if(currentInstruction.getNodeType() == Node.ELEMENT_NODE){
							Element element = (Element) currentInstruction;
							String instructionAndType = element.getAttribute("name")+";"+element.getAttribute("type");
							if(!allInstructions.contains(instructionAndType)){
								instructionAndTypeList.add(instructionAndType);
								allInstructions.add(instructionAndType);
							}
						}
					}
					categoryInfo.setInstructionAndTypeList(instructionAndTypeList);
					categoryHashMap.put(categoryName, categoryInfo);
				}
			}
			mainController.setAllInstructions(allInstructions);
			xmlInfo.setCategoryHashMap(categoryHashMap);
		} catch (ParserConfigurationException e1) {
			new ErrorDialogView("XML parser error");
		} catch (SAXException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}


	}

}

