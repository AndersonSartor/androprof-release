package models;

import java.io.File;

public class XmlInfo {
	
	private File xmlFile;
	
	public XmlInfo(File xml){
		xmlFile = xml;
	}

	public File getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	

}
