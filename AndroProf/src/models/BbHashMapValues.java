package models;

public class BbHashMapValues {
	
	private Long cost;
	private Long power;
	private String bb;
	private int instrNumber;
	private String instrList;
	
	
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public String getBb() {
		return bb;
	}
	public void setBb(String bb) {
		this.bb = bb;
	}
	public Long getPower() {
		return power;
	}
	public void setPower(Long power) {
		this.power = power;
	}
	public int getInstrNumber() {
		return instrNumber;
	}
	public void setInstrNumber(int instrNumber) {
		this.instrNumber = instrNumber;
	}
	public String getInstrList() {
		return this.instrList;
	}
	public void setInstrList(String insL) {
		this.instrList = insL;
	}

}
