package models;

public class InstructionHashMapValues {
	
	private long costCycles;
	private float costPower;
	private long counter;
	
	public InstructionHashMapValues(Long costCycles, float costPower){
		this.costCycles = costCycles;
		this.costPower = costPower;
	}

	public long getCounter() {
		return counter;
	}
	public void setCounter(long counter) {
		this.counter = counter;
	}

	public long getCostCycles() {
		return costCycles;
	}

	public void setCostCycles(long costCycles) {
		this.costCycles = costCycles;
	}

	public float getCostPower() {
		return costPower;
	}

	public void setCostPower(float costPower) {
		this.costPower = costPower;
	}

}
