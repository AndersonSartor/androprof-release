package models;

public class PidInstructionHashMapValues {
	
	private long counter;
	
	public PidInstructionHashMapValues(long count){
		this.counter = count;
	}
	
	public void setCounter(long count){
		this.counter = count;
	}
	public long getCounter(){
		return this.counter;
	}
}
