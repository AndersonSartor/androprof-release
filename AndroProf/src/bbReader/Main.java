package bbReader;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import models.SQLite;
import views.SelectArchitectureView;

public class Main {

	public static void main(String[] args) {
		
		try{
			for(LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()){
				if("Nimbus".equals(info.getName())){
					UIManager.setLookAndFeel(info.getClassName());
				}
			}
		}
		catch(Exception e){
		}
		
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			SQLite.connection = DriverManager.getConnection("jdbc:sqlite:bbDB.db");
			statement = SQLite.connection.createStatement();
			
			//joao
			
			statement.executeUpdate("create table if not exists bb(pid integer,"
					+"idx integer,"+"pc varchar(15),"+"cost integer,"+"power integer,"
					+"counter integer,"+"totalCost integer,"+"totalPower integer, bbInstrNumber integer,"
					+"primary key(pid,pc),"
					+"foreign key(idx) references bbInstrs(idx),"
					+"foreign key(pc) references bbInstrs(pc));");
			//end
			statement.executeUpdate("create table if not exists bbInstrs(idx integer,"
					+"bbInstrs text,"+"primary key(idx));");
			
			
			
			statement.executeUpdate("create table if not exists bbFilesInfo(file varchar(50),"
					+"path varchar(300),"+"primary key(file));");
			statement.executeUpdate("create table if not exists architecture(arch varchar(50),"
					+"value varchar(50),"+"primary key(arch));");
			statement.executeUpdate("create table if not exists instruction(name varchar(10),"
					+"type varchar(50),"+"architecture varchar(50),"+"category varchar(100),"
					+"counter integer,"+"primary key(name,type,architecture),"
					+"foreign key(category,architecture) references category(name,architecture));");
			
			//joao
			
			statement.executeUpdate("create table if not exists pidInstrs(pid integer,"
				    +"instrname varchar(10),"+"type varchar(50),"+"counter integer,"
					+"primary key(pid,instrname,type),"
					+"foreign key(instrname) references instruction(name)"
					+"foreign key(type) references instruction(type));");
			//end
			
			statement.executeUpdate("create table if not exists category(name varchar(100),"
					+"architecture varchar(50),"+"cycles integer,"+"power integer,"
					+"primary key(name,architecture));");
			statement.executeUpdate("create table if not exists benchmark(name varchar(100),"
					+"pid integer,"+"time integer,"+"primary key(pid));");
			statement.executeUpdate("insert or ignore into architecture values(\"selectedArch\"," +
					"\"\");");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		new SelectArchitectureView(null);
	}

}
