package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import models.InstructionInfo;
import models.SQLite;
import models.XmlInfo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import views.ImportInstrXmlView;
import views.SelectArchitectureView;

public class ImportInstrXmlController implements ActionListener{

	private ImportInstrXmlView importInstrXmlView;
	private final JFileChooser fc = new JFileChooser();
	private boolean updateComboBox = false;
	private XmlInfo xmlFile;

	public ImportInstrXmlController(ImportInstrXmlView importInstrXmlView, boolean updateComboBox){
		this.importInstrXmlView = importInstrXmlView;
		this.updateComboBox = updateComboBox;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == importInstrXmlView.getBtnBrowseXmlFIle()){
			int returnVal = fc.showOpenDialog(importInstrXmlView);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				xmlFile = new XmlInfo(file);
				importInstrXmlView.getXmlFilePath().setText(file.toString());
			} 
		}

		if(e.getSource() == importInstrXmlView.getBtnOk()){
			new Thread(new refreshThread()).start();
		}

		if(e.getSource() == importInstrXmlView.getBtnCancel()){
			importInstrXmlView.dispose();
		}

	}

	public class refreshThread implements Runnable{

		@Override
		public void run() {
			importInstrXmlView.getProgressBar().setIndeterminate(true);
			importXml();
			if(updateComboBox){
				try {
					Statement stat = SQLite.connection.createStatement();
					ResultSet res = stat.executeQuery("select distinct architecture from category");
					SelectArchitectureView.comboBoxArch.removeAllItems();
					while(res.next()){
						if(Thread.currentThread().isInterrupted()) 
							break;
						SelectArchitectureView.comboBoxArch.addItem(res.getString("architecture"));
					}
					res.close();
					stat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			importInstrXmlView.dispose();
		}

	}

	private void importXml(){
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile.getXmlFile());
			doc.getDocumentElement().normalize();

			NodeList archList = doc.getElementsByTagName("arch");

			InstructionInfo instruction = new InstructionInfo();
			PreparedStatement prep = SQLite.connection.prepareStatement("insert into instruction values(?,?,?,?,?);");
			PreparedStatement prepCat = SQLite.connection.prepareStatement("insert into category values(?,?,?,?);");

			for (int currentArchIndex = 0; currentArchIndex < archList.getLength(); currentArchIndex++) {
				Node currentArch = archList.item(currentArchIndex);
				NodeList categoryList = currentArch.getChildNodes();

				Element archEle = (Element) currentArch;
				String archName = archEle.getAttribute("name");
				instruction.setArchitecture(archName);
				
				Statement stat = SQLite.connection.createStatement();
				stat.executeUpdate("delete from instruction;");
				stat.executeUpdate("delete from category;");
				stat.close();

				for (int currentCategoryIndex = 0; currentCategoryIndex < categoryList.getLength(); currentCategoryIndex++){
					Node currentCategory = categoryList.item(currentCategoryIndex);
					NodeList instructionList = currentCategory.getChildNodes();

					Element categoryEle = (Element) currentCategory;
					instruction.setCategory(categoryEle.getAttribute("name"));
					prepCat.setString(1,instruction.getCategory());
					prepCat.setString(2, archName);
					prepCat.setInt(3, Integer.parseInt(categoryEle.getAttribute("cycles")));
					prepCat.setFloat(4, Float.parseFloat(categoryEle.getAttribute("power")));
					prepCat.addBatch();

					for(int currentInstructionIndex = 0; currentInstructionIndex < instructionList.getLength(); currentInstructionIndex++){
						Node currentInstruction = instructionList.item(currentInstructionIndex);
						if(currentInstruction.getNodeType() == Node.ELEMENT_NODE){
							Element element = (Element) currentInstruction;
							instruction.setName(element.getAttribute("name"));
							instruction.setType(element.getAttribute("type"));
							prep.setString(1, instruction.getName());
							prep.setString(2, instruction.getType());
							prep.setString(3, instruction.getArchitecture());
							prep.setString(4, instruction.getCategory());
							prep.setLong(5, 0);
							prep.addBatch();
						}
					}
				}
			}
			SQLite.connection.setAutoCommit(false);
			prep.executeBatch();
			prepCat.executeBatch();
			SQLite.connection.commit();
			SQLite.connection.setAutoCommit(true);
			prep.close();
			prepCat.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
