package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;

import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RefineryUtilities;

import views.Loading;
import views.PidCostChartView;

public class PidCycleCostChartController {

	private PidCostChartView graphView;
	private int limit;
	private Thread thread;

	public PidCycleCostChartController(PidCostChartView graphView, int limit){
		this.graphView = graphView;
		this.limit = limit;
		thread = new Thread(new getDataThread());
		thread.start();
	}

	public class getDataThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			DefaultPieDataset dataset = new DefaultPieDataset();
			try {
				Statement stat = SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select pid,SUM(totalCost) as sumTotalCost from bb group by pid "
						+"order by sumTotalCost desc");
				int limitCounter = 0;
				long totalOther = 0;
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					if(limitCounter<limit)
						dataset.setValue(res.getString("pid"), res.getLong(2));
					else
						totalOther += res.getLong(2);
					limitCounter++;
				}
				if(limitCounter>=limit){
					dataset.setValue("Others", totalOther);
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted()){
				loading.dispose();
				graphView.plotGraph(dataset);
				graphView.pack();
				RefineryUtilities.centerFrameOnScreen(graphView);
				graphView.setVisible(true);
			}
		}

	}


}
