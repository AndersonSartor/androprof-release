package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import util.Types;
import views.ErrorDialogView;
import views.ChartView;
import views.Loading;

public class BBCycleCostChartController {
	private ChartView chartView;
	private int bbPid = 0;
	private Thread thread;

	public BBCycleCostChartController(ChartView chartView, int bbPid){
		this.chartView = chartView;
		this.bbPid = bbPid;
		thread = new Thread(new getDataThread());
		thread.start();
	}

	public class getDataThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			DefaultCategoryDataset dataset = null;
			try {
				dataset = new DefaultCategoryDataset();
				Statement stat = SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select pc,totalCost from bb where pid="+bbPid+" order by totalCost desc");
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					dataset.addValue(res.getLong("totalCost"), "BB", res.getString("pc"));
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted()){
				if(dataset.getColumnCount()!=0){
					chartView.plotGraph(dataset, Types.Elem.BB, "PID "+bbPid, "PC", "Cycle cost");
					chartView.pack();
					RefineryUtilities.centerFrameOnScreen(chartView);
					chartView.setVisible(true);
				} else
					new ErrorDialogView("PID is not valid.");
				loading.dispose();
			}
		}
	}

}
