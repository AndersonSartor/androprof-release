package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import util.QueryClause;
import util.Types;
import views.ChartView;
import views.Loading;

public class CategoryPowerCostChartController {
	private ChartView chartView;
	private String arch;
	private String instrType;
	private Thread thread;
	private int pid;

	public CategoryPowerCostChartController(ChartView chartView, String arch,
			String instrType,int pid){
		this.chartView = chartView;
		this.arch = arch;
		this.instrType = instrType;
		this.pid = pid;
		thread = new Thread(new getDataThread());
		thread.start();
	}

	public class getDataThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			DefaultCategoryDataset dataset = null;
			try {
				dataset = new DefaultCategoryDataset();
				Statement stat = SQLite.connection.createStatement();
				String query = "select category,sum(counter*power) as categoryCost" 
						+" from (select instrname as name,counter,type from pidInstrs where"

						+ QueryClause.formatWherePid(pid, instrType)
						+ ") NATURAL JOIN (select instruction.name,category,type,power from instruction ,category where" 
						+" instruction.category = category.name and instruction.architecture = category.architecture and"
						+ QueryClause.formatWhereCategories(arch, instrType,"instruction")
				        + ") group by category order by categoryCost desc";
				ResultSet res = stat.executeQuery(query);
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					dataset.addValue(res.getLong("categoryCost"), "Category", res.getString("category"));
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted()){
				chartView.plotGraph(dataset,Types.Elem.CATEG, "Instruction Categories Total Power Cost", "Category", "Power Cost");
				chartView.pack();
				RefineryUtilities.centerFrameOnScreen(chartView);
				chartView.setVisible(true);
				loading.dispose();
			}
		}
	}
}
