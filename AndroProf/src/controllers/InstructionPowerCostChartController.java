package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import util.QueryClause;
import util.Types;
import views.ChartView;
import views.Loading;

public class InstructionPowerCostChartController {
	private ChartView chartView;
	private String arch;
	private String instrType;
	private double perc;
	private Thread thread;
	private int pid;

	public InstructionPowerCostChartController(ChartView chartView, String arch,
			String instrType,int pid, double perc){
		this.chartView = chartView;
		this.arch = arch;
		this.instrType = instrType;
		this.perc = perc;
		this.pid = pid;
		thread = new Thread(new getDataThread());
		thread.start();
	}

	public class getDataThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			DefaultCategoryDataset dataset = null;
			long max = 0;
			try {
				dataset = new DefaultCategoryDataset();
				
				
	//joao 
				
				
				Statement stat = SQLite.connection.createStatement();

				dataset = new DefaultCategoryDataset();
				
				String outQuery = "select name,category,type,counter,power from (";

				String firstQuery = "select instrname as name,type,counter from pidInstrs where";
				String firstWhere = QueryClause.formatWherePid(pid, instrType);
				
				String join = ") NATURAL JOIN(";
				
				String secondQuery = "select instruction.name as name,category,power,type from instruction,category where instruction.category = category.name"
						+" and category.architecture = " + "\"" +arch +"\"" +" and ";
						String secondWhere = "instruction.architecture = " + "\"" +arch +"\"" + " and instruction.type = ";
						if(instrType == "Both"){
							secondWhere +=  "\""+"Thumb"+ "\"" +" or instruction.type =" + "\""+"ARM"+ "\"";
						}
						else secondWhere += "\""+instrType+ "\"";
							
						
				
				String order = ") order by counter desc;";
				
				String fullQuery = outQuery + firstQuery + firstWhere + join + secondQuery + secondWhere  + order;
				
				ResultSet resMax = stat.executeQuery("select max(counter) as max from pidInstrs where"+firstWhere);
				max = resMax.getLong("max");
				resMax.close();
				
				System.out.println(fullQuery);
				
				ResultSet res = stat.executeQuery(fullQuery);
				
				
				
		/*		Statement stat = SQLite.connection.createStatement();
				String query = "select instruction.name as name,counter,category,type,power from instruction,category where";
				String where = "";
				where += " instruction.architecture = \"" + arch + "\"";
				where += " and category.architecture = \"" + arch + "\"";
				where += QueryClause.formatWhereCategories("", instrType);
				where += " and instruction.category = category.name";
				where += " order by counter*power desc";
				ResultSet resMax = stat.executeQuery("select max(counter) as max from instruction,category where"+where);
				max = resMax.getLong("max");
				resMax.close();
				query+=where;
				ResultSet res = stat.executeQuery(query);*/
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					if(res.getLong("counter") >= (perc/100)*max)
						dataset.addValue(res.getLong("counter")*res.getLong("power"), "Instruction", 
								res.getString("name")+";"+res.getString("type"));
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted()){
				loading.dispose();
				chartView.plotGraph(dataset,Types.Elem.INSTR, "Instructions total power cost", "Instruction", "Power cost");
				chartView.pack();
				RefineryUtilities.centerFrameOnScreen(chartView);
				chartView.setVisible(true);
			}
		}
	}
}
