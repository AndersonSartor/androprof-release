package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import util.QueryClause;
import util.Types;
import views.ChartView;
import views.Loading;

public class InstructionHistogramController {

	private ChartView chartView;
	private String arch;
	private int pid;
	private String instrType;
	private double perc;
	private Thread thread;

	public InstructionHistogramController(ChartView chartView, String arch,int pid,
			String instrType, double perc){
		this.chartView = chartView;
		this.arch = arch;
		this.pid = pid;
		this.instrType = instrType;
		this.perc = perc;
		thread = new Thread(new getDataThread());
		thread.start();
	}

	public class getDataThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			DefaultCategoryDataset dataset = null;
			long max = 0;
			try {
				
				//joao 
				
				
				Statement stat = SQLite.connection.createStatement();

				dataset = new DefaultCategoryDataset();
				
				String outQuery = "select name,category,type,counter from (";

				String firstQuery = "select instrname as name,type,counter from pidInstrs where";
				String firstWhere = QueryClause.formatWherePid(pid, instrType);
				
				String join = ") NATURAL JOIN(";
				
				
				String secondQuery = "select name,category,type from instruction where";
				String secondWhere = QueryClause.formatWhereCategories(arch, instrType);
				
				String order = ") order by counter desc;";
				
				String fullQuery = outQuery + firstQuery + firstWhere + join + secondQuery + secondWhere  + order;
				
				ResultSet resMax = stat.executeQuery("select max(counter) as max from pidInstrs where"+firstWhere);
				max = resMax.getLong("max");
				resMax.close();
				
				System.out.println(fullQuery);
				
				ResultSet res = stat.executeQuery(fullQuery);
				
				
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					if(res.getLong("counter") >= (perc/100)*max){
						dataset.addValue(res.getLong("counter"), "Instruction", 
								res.getString("name")+";"+res.getString("type"));
					}
				}
				
				
				//end
				
				/*
				
				Statement stat = SQLite.connection.createStatement();
				String query = "select name,counter,category,type from instruction where";
				String where = QueryClause.formatWhereCategories(arch, instrType);
				where +=  " order by counter desc";
				ResultSet resMax = stat.executeQuery("select max(counter) as max from instruction where"+where);
				max = resMax.getLong("max");
				resMax.close();
				query+=where;
				
				ResultSet res = stat.executeQuery(query);
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					if(res.getLong("counter") >= (perc/100)*max){
						dataset.addValue(res.getLong("counter"), "Instruction", 
								res.getString("name")+";"+res.getString("type"));
					}
				}
				*/
				
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted()){
				loading.dispose();
				chartView.plotGraph(dataset,Types.Elem.INSTR, "Instructions Histogram", "Instruction", "Frequency");
				chartView.pack();
				RefineryUtilities.centerFrameOnScreen(chartView);
				chartView.setVisible(true);
			}
		}
	}

}
