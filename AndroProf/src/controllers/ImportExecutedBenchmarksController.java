package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Scanner;

import javax.swing.JFileChooser;

import models.SQLite;
import views.ImportInstrXmlView;

public class ImportExecutedBenchmarksController implements ActionListener{

	private ImportInstrXmlView view;
	private final JFileChooser fc = new JFileChooser();
	private MainController mainController;
	private String benchmarksFilePath;

	public ImportExecutedBenchmarksController(ImportInstrXmlView view, MainController mainController){
		this.view = view;
		this.mainController = mainController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == view.getBtnBrowseXmlFIle()){
			int returnVal = fc.showOpenDialog(view);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				view.getXmlFilePath().setText(file.toString());
				benchmarksFilePath = file.toString();
			} 
		}

		if(e.getSource() == view.getBtnOk()){
			new Thread(new refreshThread()).start();
		}

		if(e.getSource() == view.getBtnCancel()){
			view.dispose();
		}

	}

	public class refreshThread implements Runnable{

		@Override
		public void run() {
			view.getProgressBar().setIndeterminate(true);
			importExecutedBenchamrks();
			if(mainController != null)
				mainController.refreshWindow(false);
			view.dispose();
		}

	}

	private void importExecutedBenchamrks(){
		try{
			Statement stat = SQLite.connection.createStatement();
			stat.executeUpdate("delete from benchmark;");
			stat.close();
			
			PreparedStatement prep = SQLite.connection.prepareStatement("insert into benchmark values(?,?,?);");

			FileInputStream logFile = null;
			try {
				logFile = new FileInputStream(benchmarksFilePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			Scanner scanner = new Scanner(logFile);


			while(scanner.hasNextLine()){
				String currentLine = scanner.nextLine();
				String[] elements = currentLine.split(";");
				prep.setString(1, elements[0]);
				try{
					prep.setInt(2, Integer.valueOf(elements[1].substring(elements[1].indexOf(":")+1)));
				}
				catch(NumberFormatException ex){
					prep.setInt(2, 0);
				}
				try{
					prep.setLong(3, Long.valueOf(elements[2].substring(elements[2].indexOf(":")+1)));
				}
				catch(NumberFormatException ex){
					prep.setLong(3, 0);
				}
				prep.addBatch();
			}
			SQLite.connection.setAutoCommit(false);
			prep.executeBatch();
			SQLite.connection.commit();
			SQLite.connection.setAutoCommit(true);
			prep.close();

			scanner.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
