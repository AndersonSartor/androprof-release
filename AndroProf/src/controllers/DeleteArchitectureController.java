package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;
import views.DeleteArchitectureView;
import views.ErrorDialogView;
import views.Loading;

public class DeleteArchitectureController implements ActionListener{

	private DeleteArchitectureView deleteArchitectureView;
	private Thread threadDelete;
	private Thread threadRefresh;

	public DeleteArchitectureController(DeleteArchitectureView DeleteArchitectureView){
		this.deleteArchitectureView = DeleteArchitectureView;

		threadRefresh = new Thread(new refreshComboThread());
		threadRefresh.start();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == deleteArchitectureView.getBtnOk()){
			if(!deleteArchitectureView.getComboBoxArch().isEmpty()){
				threadDelete = new Thread(new deleteArchThread());
				threadDelete.start();
			}
			else
				new ErrorDialogView("No architecture selected");
		}

		if(e.getSource() == deleteArchitectureView.getBtnCancel()){
			deleteArchitectureView.dispose();
		}

	}

	public class refreshComboThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(threadRefresh);
			try {
				Statement stat = SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select distinct architecture from instruction");
				while(res.next()){
					if(Thread.currentThread().isInterrupted()) 
						break;
					deleteArchitectureView.comboBoxArch.addItem(res.getString("architecture"));
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted())
				loading.dispose();
		}

	}

	public class deleteArchThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(threadDelete);
			try {
				Statement stat = SQLite.connection.createStatement();
				stat.executeUpdate("delete from category where architecture=\""
						+deleteArchitectureView.getComboBoxArch()+"\";");
				stat.executeUpdate("delete from instruction where architecture=\""
						+deleteArchitectureView.getComboBoxArch()+"\";");
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			deleteArchitectureView.dispose();
			if(!Thread.currentThread().isInterrupted()){
				loading.dispose();
			}
		}

	}


}
