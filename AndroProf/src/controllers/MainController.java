package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import javax.swing.SortOrder;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.DefaultTableModel;

import models.BbHashMapValues;
import models.InstructionHashMapValues;
import models.PidInstructionHashMapValues;
import models.SQLite;
import util.Types;
import util.Types.Arch;
import views.DeleteArchitectureView;
import views.ErrorDialogView;
import views.ExecutedBenchmarksView;
import views.ImportBBsView;
import views.ImportInstrXmlView;
import views.Loading;
import views.MainView;
import views.SelectArchitectureView;
import views.SelectLimitPidsView;
import views.SelectOptionsView;
import views.SelectPercToDisplayView;
import views.SelectPidView;

public class MainController implements ActionListener, WindowListener{

	private final int MAX_ROWS = 10000;
	private final int BUFFER = 100000;
	private long currentPage = 0;
	public MainView mainView;
	private MainController context;
	private Map<Long, BbHashMapValues> bbCostHashTable;
	private Thread thread;
	private String filter = "";
	private boolean running = false;

	public MainController(MainView mainView){
		this.mainView = mainView;
		this.context = this;
		refreshFilesPath();
		updateTable();
	}

	public void refreshWindow(boolean updateDB){
		try {
			Statement stat = SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select * from bbFilesInfo");
			while(res.next()){
				if(res.getString("file").equals("bbLog"))
					mainView.getBbLogPath().setText(res.getString("path"));
				if(res.getString("file").equals("bbInfo"))
					mainView.getBbInfoPath().setText(res.getString("path"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(updateDB){
			bbCostHashTable = new Hashtable<Long, BbHashMapValues>();
			mergeFiles(mainView.getBbLogPath().getText(), mainView.getBbInfoPath().getText());
		}

		updateTable();
	}

	private void mergeFiles(String bbLogPath, String bbInfoPath){
		calculateCost(bbLogPath);
		saveBbInfoToDb(bbInfoPath);
	}

	private void refreshFilesPath(){
		try {
			Statement stat = SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select path from bbFilesInfo where file='bbLog';");
			if(res.next()){
				mainView.getBbLogPath().setText(res.getString("path"));
			}
			res = stat.executeQuery("select path from bbFilesInfo where file='bbInfo';");
			if(res.next())
				mainView.getBbInfoPath().setText(res.getString("path"));
			res.close();
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateTable(){
		thread = new Thread(new updateTableThread());
		thread.start();
	}

	private void saveBbInfoToDb(String bbInfoPath){
		FileInputStream bbInfoFile = null;
		try {
			bbInfoFile = new FileInputStream(bbInfoPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner bbInfoScanner = new Scanner(bbInfoFile);
		PreparedStatement prep = null;
		try {
			prep = SQLite.connection.prepareStatement("insert into bb values(?,?,?,?,?,?,?,?,?);");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		PreparedStatement prepBb = null;
		try {
			prepBb = SQLite.connection.prepareStatement("insert or ignore into bbInstrs values(?,?)");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		//joao
		
		PreparedStatement prepPid = null;
		try {
			prepPid = SQLite.connection.prepareStatement("insert or ignore into pidInstrs values(?,?,?,?)");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		//end
		
		int batchCount = 0;
		
		//joao 
		
		HashMap<String,PidInstructionHashMapValues> pidHash = new HashMap<String,PidInstructionHashMapValues>();
		
		//end
		
		while(bbInfoScanner.hasNextLine()){
			

			
			String bbInfoLine = bbInfoScanner.nextLine();
			String[] info = bbInfoLine.split(",");

			String pc="";
			int pid=0;
			long counter, bbIdx;
			try {
				pc = info[0];
				pid = Integer.parseInt(info[1]);
				counter = Long.parseLong(info[2]);
				bbIdx = Long.parseLong(info[3]);
			} catch (Exception e) {
				new ErrorDialogView("Wrong BB info format");
				pid=0;
				counter=0;
				bbIdx=0;
			}

			long cost;
			long power;
			String bb = null;
			int bbInstrNumber;
			
		    String[] instrNames;
			
			try {
				BbHashMapValues values = bbCostHashTable.get(bbIdx);
				cost = values.getCost();
				power = values.getPower();
				bb = values.getBb();
				bbInstrNumber = values.getInstrNumber();
				instrNames = values.getInstrList().split(",");
			} catch (Exception e) {
				System.out.println(bbIdx);
				cost=0;
				power = 0;
				bbInstrNumber = 0;
				instrNames = null;
			}
			
			//joao
			
			try{
				for(int i = 0;i < instrNames.length;i++){
					String pidKey = String.valueOf(pid) + ";" + instrNames[i];
					if(pidHash.containsKey(pidKey)){
						PidInstructionHashMapValues pidValue = pidHash.get(pidKey);
						pidValue.setCounter(pidValue.getCounter() + counter);
					}
					else{		
					PidInstructionHashMapValues pidValue = new PidInstructionHashMapValues(counter);
					pidHash.put(pidKey, pidValue);
					}
				}
			}
			catch(NullPointerException epid){
				epid.printStackTrace();
			}
			
			//end
			
			
			try {
				prep.setInt(1, pid);
				prep.setLong(2, bbIdx);
				prep.setString(3, pc);
				prep.setLong(4, cost);
				prep.setLong(5, power);
				prep.setLong(6, counter);
				prep.setLong(7, cost*counter);
				prep.setLong(8, power*counter);
				prep.setInt(9, bbInstrNumber);
				prepBb.setLong(1, bbIdx);
				prepBb.setString(2, bb);
				prepBb.addBatch();
				prep.addBatch();
				batchCount++;
			} catch (SQLException e) {
				System.out.print("dsdsadsadsadsa");
				e.printStackTrace();
			}

			if(batchCount>=BUFFER){
				try {
					SQLite.connection.setAutoCommit(false);
					prep.executeBatch();
					prepBb.executeBatch();
					prep.clearBatch();
					prepBb.clearBatch();
					SQLite.connection.commit();
					SQLite.connection.setAutoCommit(true);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				batchCount = 0;
			}

		}
		
		//joao
		
		if(batchCount > 0){
	
			try {
				SQLite.connection.setAutoCommit(false);
				prep.executeBatch();
				prepBb.executeBatch();
				prep.clearBatch();
				prepBb.clearBatch();
				SQLite.connection.commit();
				SQLite.connection.setAutoCommit(true);
			}catch (SQLException e) {
				e.printStackTrace();
			}
			batchCount = 0;
		}
		try {
			prep.close();
			prepBb.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		Set<Entry<String,PidInstructionHashMapValues>> entry = pidHash.entrySet();
		Iterator<Entry<String,PidInstructionHashMapValues>> it = entry.iterator();
		while(it.hasNext()){
			Map.Entry<String,PidInstructionHashMapValues> instr = (Map.Entry<String,PidInstructionHashMapValues>)it.next();
			String key = instr.getKey();
			PidInstructionHashMapValues value = instr.getValue();
			try{
			    String[] keys = key.split(";");
				prepPid.setInt(1, Integer.parseInt(keys[0]));
				prepPid.setString(2,keys[1]);
				prepPid.setString(3, keys[2]);
				prepPid.setLong(4, value.getCounter());
				prepPid.addBatch();
				batchCount++;
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			
			if(batchCount>=BUFFER){
				try {
					SQLite.connection.setAutoCommit(false);
					prepPid.executeBatch();
					prepPid.clearBatch();
					SQLite.connection.commit();
					SQLite.connection.setAutoCommit(true);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				batchCount = 0;
			}
		}
		
		if(batchCount > 0){
			try {
				SQLite.connection.setAutoCommit(false);
				prepPid.executeBatch();
				prepPid.clearBatch();
				SQLite.connection.commit();
				SQLite.connection.setAutoCommit(true);
			}catch (SQLException e) {
				e.printStackTrace();
			}
			batchCount = 0;
		}
		
		try {
			prepPid.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		bbInfoScanner.close();
		
		
		//end
		
	}

	public void calculateCost(String filePath){
		String arch = "";
		String missingInstrs = "";
		try {
			Statement stat = SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select value from architecture where arch=\"selectedArch\";");
			if(res.next()){
				arch = res.getString("value");
			}
			res.close();
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		HashMap<String, InstructionHashMapValues> instrHashMap = new HashMap<String, InstructionHashMapValues>();
		try {
			Statement stat = SQLite.connection.createStatement();
			Statement statBb = SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select name, category, type, architecture, counter from instruction where architecture=\""+arch+"\"");
			while(res.next()){
				ResultSet resBb = statBb.executeQuery("select cycles, power from category where name=\""+res.getString("category")+"\"" +
						" and architecture=\""+res.getString("architecture")+"\"");
				if(resBb.next()){
					InstructionHashMapValues values = new InstructionHashMapValues(resBb.getLong("cycles"), resBb.getFloat("power"));
					instrHashMap.put(res.getString("name")+";"+res.getString("type")+";"+res.getString("architecture"), values);
				}
				resBb.close();
			}
			res.close();
			stat.close();
			statBb.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		FileInputStream logFile = null;
		try {
			logFile = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner scanner = new Scanner(logFile);

		Types.Arch selectedArch = null;
		if(arch.toLowerCase().startsWith("arm"))
			selectedArch = Arch.ARM;
		else if(arch.toLowerCase().startsWith("x86"))
			selectedArch = Arch.x86;
		else if(arch.toLowerCase().startsWith("mips"))
			selectedArch = Arch.MIPS;
		while(scanner.hasNextLine()){
			String currentLine = scanner.nextLine();
			if(currentLine.equals("----------------")){
				
				
				String bbIdxLine = scanner.nextLine();
				String bbLine = "";
				String bb = "";
				String instL = "";
				long bbCost = 0;
				long bbPower = 0;
				long bbIdx = 0; 
				int bbInstrNumber = 0;
				int index = bbIdxLine.lastIndexOf(":");
				int lastIndex = bbIdxLine.indexOf(")");
				try {
					bbIdx = Integer.parseInt(bbIdxLine.substring(index+1,lastIndex));
				} catch (NumberFormatException e) {
					bbIdx = 0;
				}

				do
				{
					bbLine = scanner.nextLine();
					if(!bbLine.isEmpty()){
						bbInstrNumber++;
						String[] instrLine = bbLine.split("\\s+");
						String instr = "";
						String type = "Unknown";
						
						if(selectedArch == Arch.ARM){
							if(instrLine[1].length()==8){
								
								//joao
								
								instr = instrLine[2];
								
								//end
								
								type = "ARM";
							}
							else if(instrLine[1].length()==4){
								String substr = bbLine.substring(18);
								if(substr.indexOf(instrLine[2]) == 0){
									//instrCode = instrLine[1] + " " + instrLine[2];
									instr = instrLine[3];
								}
								else{
									//instrCode = instrLine[1];
									instr = instrLine[2];
								}
								type = "Thumb";
							}
							
							//joao
							
							if(instL != "")
							    instL += ",";
					
							instL += instr + ";" + type;
							
							//end
						}
						else{
							instr = instrLine[1];
							if(selectedArch == Arch.x86)
								type = "x86";
							else if(selectedArch == Arch.MIPS)
								type = "MIPS";
							
							if(instL != "")
							    instL += ",";
					
							instL += instr + ";" + type;
						}
						bb += bbLine + "\n";

						String key = instr+";"+type+";"+arch;
						InstructionHashMapValues values = instrHashMap.get(key);
						if(values!=null){
							long counter = values.getCounter();
							bbCost += values.getCostCycles();
							bbPower += values.getCostPower();
							counter++;
							values.setCounter(counter);
							instrHashMap.put(key, values);
						}
						else{
							InstructionHashMapValues val = new InstructionHashMapValues((long)0, 0);
							instrHashMap.put(key, val);
							if(missingInstrs.isEmpty())
								missingInstrs+=instr;
							else
								missingInstrs+=","+instr;
						}

					}
				}
				while(!bbLine.isEmpty());
				BbHashMapValues values = new BbHashMapValues();
				values.setCost(bbCost);
				values.setPower(bbPower);
				values.setBb(bb);
				values.setInstrNumber(bbInstrNumber);
				
				//joao
				
				values.setInstrList(instL);
				
				//end
				
				bbCostHashTable.put(bbIdx, values);
			
				}
		}
		if(!missingInstrs.isEmpty()){
			new ErrorDialogView("Missing instructions in the XML file: " + missingInstrs);
		}
		scanner.close();
		
		
		
		Set<Entry<String, InstructionHashMapValues>> p=instrHashMap.entrySet();
		Iterator<Entry<String, InstructionHashMapValues>> instrIt=p.iterator();
		PreparedStatement prep = null;
		try {
			prep = SQLite.connection.prepareStatement("update instruction set counter =? where name=? and " 
					+ "type=? and architecture=?");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		while(instrIt.hasNext())
		{
			Map.Entry<String, InstructionHashMapValues> instr =(Map.Entry<String, InstructionHashMapValues>)instrIt.next();
			String key = instr.getKey();
			String[] keyElem = key.split(";");
			InstructionHashMapValues values = instr.getValue();
			try {
				prep.setLong(1, values.getCounter());
				prep.setString(2, keyElem[0]);
				prep.setString(3, keyElem[1]);
				prep.setString(4, keyElem[2]);
				prep.addBatch();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		try {
			SQLite.connection.setAutoCommit(false);
			prep.executeBatch();
			prep.clearBatch();
			SQLite.connection.commit();
			SQLite.connection.setAutoCommit(true);
			prep.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == mainView.getMntmImportBbFiles()){
			ImportBBsView importBBsView = new ImportBBsView();
			ImportBBsController importBBsController = new ImportBBsController(importBBsView, this);
			importBBsView.setController(importBBsController);
		}
		if(e.getSource() == mainView.getMntmImportExecutedBenchmarks()){
			ImportInstrXmlView importExecutedBenchmarksView = new ImportInstrXmlView();
			importExecutedBenchmarksView.setTitle("Import executed benchmarks");
			importExecutedBenchmarksView.setLblInstructionsXmlFile("Executed benchmarks file:");
			ImportExecutedBenchmarksController importExecutedBenchmarksController = new ImportExecutedBenchmarksController(importExecutedBenchmarksView, context);
			importExecutedBenchmarksView.setControllerBenchmarks(importExecutedBenchmarksController);
		}
		if(e.getSource() == mainView.getMntmDeleteArchitecture()){
			DeleteArchitectureView deleteArchitectureView = new DeleteArchitectureView();
			DeleteArchitectureController deleteArchitectureController = new DeleteArchitectureController(deleteArchitectureView);
			deleteArchitectureView.setController(deleteArchitectureController);
		}
		if(e.getSource() == mainView.getBtnFilter()){
			currentPage = 0;
			boolean error = false;
			boolean init = false;
			String filter = "where ";
			String pidFilter = mainView.getPidFilter().getText();
			if(!pidFilter.isEmpty()){
				try{
					int pidFilterInt = Integer.parseInt(pidFilter);
					filter += "pid="+pidFilterInt;
					init = true;
				}
				catch(NumberFormatException ex){
					new ErrorDialogView("PID must be an integer");
					error = true;
				}
			}
			String bbIdFilter = mainView.getBbIdFilter().getText();
			if(!bbIdFilter.isEmpty()){
				try{
					int bbIdFilterInt = Integer.parseInt(bbIdFilter);
					if(init)
						filter += " and idx="+bbIdFilterInt;
					else
						filter += "idx="+bbIdFilterInt;
					init = true;
				}
				catch(NumberFormatException ex){
					new ErrorDialogView("PID must be an integer");
					error = true;
				}
			}
			if(filter.equals("where "))
				this.filter = "";
			else
				this.filter = filter;
			if(!error)
				updateTable();
		}
		if(e.getSource() == mainView.getBtnPrevPage()){
			if(currentPage-MAX_ROWS>=0){
				currentPage-=MAX_ROWS;
				updateTable();
			}
		}
		if(e.getSource() == mainView.getBtnNextPage()){
			ResultSet res;
			long count = 0;
			try {
				Statement stat = SQLite.connection.createStatement();
				res = stat.executeQuery("select count(*) from bb;");
				if(res.next()){
					count = res.getLong(1);
				}
				res.close();
				stat.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			if(currentPage+MAX_ROWS<=count){
				currentPage+=MAX_ROWS;
				updateTable();
			}
		}
		if(e.getSource() == mainView.getMntmSelectArchitecture()){
			new SelectArchitectureView(context);
		}
		if(e.getSource() == mainView.getMntmDeleteExecutedBenchmarks()){
				deleteExecutedBenchmarks();
		}
		if(e.getSource() == mainView.getMntmBenchmarks()){
			new ExecutedBenchmarksView();
		}
		if(e.getSource() == mainView.getMntmCycleCostPid()){			
			new SelectLimitPidsView(Types.Cost.CYCLES);
		}
		if(e.getSource() == mainView.getMntmPowerCostPid()){			
			new SelectLimitPidsView(Types.Cost.POWER);
		}
		if(e.getSource() == mainView.getMntmBbHistogram()){
			new SelectPidView(Types.NextView.BB_HISTOGRAM);
		}
		if(e.getSource() == mainView.getMntmBbTotalCycle()){
			new SelectPidView(Types.NextView.BB_TOTAL_CYCLES);
		}
		if(e.getSource() == mainView.getMntmBbsTotalPower()){
			new SelectPidView(Types.NextView.BB_TOTAL_POWER);
		}
		if(e.getSource() == mainView.getMntmInstructionHistogram()){
			new SelectPercToDisplayView(Types.NextView.INSTRUCTION_HISTOGRAM);
		}
		if(e.getSource() == mainView.getMntmInstructionTotalCycles()){
			new SelectPercToDisplayView(Types.NextView.INSTRUCTION_TOTAL_CYCLE);
		}
		if(e.getSource() == mainView.getMntmInstructionsTotalPower()){
			new SelectPercToDisplayView(Types.NextView.INSTRUCTION_TOTAL_POWER);
		}
		if(e.getSource() == mainView.getMntmCategoryHistogram()){
			new SelectOptionsView(Types.NextView.CATEGORY_HISTOGRAM,0);
		}
		if(e.getSource() == mainView.getMntmCategoryTotalCycles()){
			new SelectOptionsView(Types.NextView.CATEGORY_TOTAL_CYCLES,0);
		}
		if(e.getSource() == mainView.getMntmCategoriesTotalPower()){
			new SelectOptionsView(Types.NextView.CATEGORY_TOTAL_POWER,0);
		}
		if(e.getSource() == mainView.getMntmPerformance()){
			new SelectPidView(Types.NextView.PERFORMANCE_ESTIMATION);
		}

	}

	private void deleteExecutedBenchmarks(){
		try {
			Statement stat = SQLite.connection.createStatement();
			stat.executeUpdate("delete from benchmark;");
			stat.close();
			context.refreshWindow(false);
			ErrorDialogView dialog = new ErrorDialogView("Benchmarks succesfully removed");
			dialog.setTitle("Success");
		} catch (SQLException e) {
			new ErrorDialogView("Benchmarks were not removed");
		}
	}
	
	public RowSorterListener rowSorterListener = new RowSorterListener() {

		@Override
		public void sorterChanged(RowSorterEvent e) {
			currentPage = 0;
			updateTable();
		}
	};

	public class updateTableThread implements Runnable{

		@Override
		public void run() {
			if(!running){
				running = true;
				Loading loading = null;
				try {
					loading = new Loading(thread);
					Statement stat = SQLite.connection.createStatement();
					DefaultTableModel dtm = (DefaultTableModel) mainView.table.getModel();
					String sortOrder="";
					if(mainView.table.getRowSorter().getSortKeys().get(0).getSortOrder() == SortOrder.DESCENDING)
						sortOrder = "desc";
					else if(mainView.table.getRowSorter().getSortKeys().get(0).getSortOrder() == SortOrder.ASCENDING)
						sortOrder = "asc";
					ResultSet res;
					String columnSorter = "totalCost";
					switch(mainView.table.getRowSorter().getSortKeys().get(0).getColumn()){
					case 0:
						columnSorter = "pid";
						break;
					case 1:
						columnSorter = "idx";
						break;
					case 2:
						columnSorter = "pc";
						break;
					case 3:
						columnSorter = "cost";
						break;
					case 4:
						columnSorter = "power";
						break;
					case 5:
						columnSorter = "counter";
						break;
					case 6:
						columnSorter = "totalCost";
						break;
					case 7:
						columnSorter = "totalPower";
						break;
					}
					if(sortOrder.isEmpty()){
						res = stat.executeQuery("select * from bb "+filter+" limit "+currentPage+","+((long)(currentPage+MAX_ROWS)));
					}
					else{
						res = stat.executeQuery("select * from bb "+filter+" order by "+columnSorter+" "+sortOrder+" limit "+currentPage+","
								+((long)(currentPage+MAX_ROWS)));
					}
					dtm.setRowCount(0);
					int rowCounter=0;
					while(res.next() && rowCounter<MAX_ROWS){
						if(Thread.currentThread().isInterrupted()) 
							break;
						dtm.addRow(new Object[]{res.getInt("pid"),res.getLong("idx"), res.getString("pc"),
								res.getLong("cost"), res.getLong("power"), 
								res.getLong("counter"), res.getLong("totalCost"), res.getLong("totalPower")});
						rowCounter++;
					}
					res.close();
					stat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				updateBenchmarksTable();
				
				if(!Thread.currentThread().isInterrupted())
					loading.dispose();
				running = false;
			}
		}

	}
	
	private void updateBenchmarksTable(){
		try {
			Statement stat= SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select * from benchmark");
			DefaultTableModel dtm = (DefaultTableModel) mainView.getBenchmarksTable().getModel();
			int rowCount = 0;
			while(res.next()){
				String benchmark = res.getString("name");
				int pid = res.getInt("pid");
				if(Thread.currentThread().isInterrupted())
					return;
				dtm.addRow(new Object[]{benchmark, pid});
				rowCount++;
			}
			if(rowCount == 0)
				mainView.getBenchmarksScrollPane().setVisible(false);
			else
				mainView.getBenchmarksScrollPane().setVisible(true);
			mainView.getContentPane().validate();
			mainView.getContentPane().repaint();
			res.close();
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		try {
			SQLite.connection.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}


}
