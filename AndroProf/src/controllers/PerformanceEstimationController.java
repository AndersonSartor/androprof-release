package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.SQLite;
import views.Loading;
import views.PerformanceEstimationView;

public class PerformanceEstimationController implements ActionListener{

	private PerformanceEstimationView performanceEstimationView;
	private int pid = 0;
	private Thread thread;

	public PerformanceEstimationController(PerformanceEstimationView performanceEstimationView, int pid){
		this.performanceEstimationView = performanceEstimationView;
		this.pid = pid;
	}
	public class calculateThread implements Runnable{

		@Override
		public void run() {
			Loading loading = new Loading(thread);
			long numInstrs = 0;
			try {
				Statement stat = SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select sum(bbInstrNumber*counter) as numInstrs from " +
						"bb where pid="+pid+";");
				if(res.next()){
					numInstrs = res.getLong("numInstrs");
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if(!Thread.currentThread().isInterrupted()){
				long freqHz = (long) (performanceEstimationView.getOperationFrequency()*1000000000);
				double cpi = performanceEstimationView.getCpi();
				long baseNumInstrs = performanceEstimationView.getBaseNumInstructions();
				long baseNumCacheMisses = performanceEstimationView.getBaseNumCacheMisses();
				double procPwDis = performanceEstimationView.getProcPwDis();
				double memAcEnCons = performanceEstimationView.getMemAcEnCons()/1000000000;
				
				double estimCycles = numInstrs*cpi;
				
				try{
					double estimTime = estimCycles/freqHz;
					double memAcRate = (double)baseNumCacheMisses/(double)baseNumInstrs;
					double memAcPwDis = (memAcEnCons*memAcRate*numInstrs)/estimTime;
					double estimPwDis = procPwDis + memAcPwDis;
					double estimEnCons = estimPwDis*estimTime;
					performanceEstimationView.setResult(estimTime);
					performanceEstimationView.setEstimPowerDis(estimPwDis);
					performanceEstimationView.setEstimEnergyCons(estimEnCons);
				}
				catch(Exception e){
					performanceEstimationView.setResult(0);
					performanceEstimationView.setEstimPowerDis(0);
					performanceEstimationView.setEstimEnergyCons(0);
				}
				
				loading.dispose();
			}
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == performanceEstimationView.getBtnCalculate()){
			if(performanceEstimationView.getOperationFrequency()!=0){
				thread = new Thread(new calculateThread());
				thread.start();
			}
		}

	}

}
