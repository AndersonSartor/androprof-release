package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFileChooser;

import models.SQLite;
import views.ImportBBsView;
import views.MainView;

public class ImportBBsController implements ActionListener{

	private ImportBBsView importBBsView;
	private final JFileChooser fc = new JFileChooser();
	private MainController mainController;

	public ImportBBsController(ImportBBsView loadBBsView, MainController mainController){
		this.importBBsView = loadBBsView;
		this.mainController = mainController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == importBBsView.getBtnLoadBbLog()){
			int returnVal = fc.showOpenDialog(importBBsView);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				importBBsView.getBbLogPath().setText(file.toString());
			} 
		}

		if(e.getSource() == importBBsView.getBtnLoadBbInfo()){
			int returnVal = fc.showOpenDialog(importBBsView);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				importBBsView.getBbInfoPath().setText(file.toString());
			} 
		}

		if(e.getSource() == importBBsView.getBtnOk()){
			try {
				Statement stat = SQLite.connection.createStatement();
				stat.executeUpdate("delete from bb");
				stat.executeUpdate("delete from benchmark;");
				PreparedStatement prep = SQLite.connection.prepareStatement("insert or replace into bbFilesInfo values(?,?);");
				prep.setString(1, "bbLog");
				prep.setString(2, importBBsView.getBbLogPath().getText());
				prep.addBatch();
				prep.setString(1, "bbInfo");
				prep.setString(2, importBBsView.getBbInfoPath().getText());
				prep.addBatch();
				stat.close();
				SQLite.connection.setAutoCommit(false);
				prep.executeBatch();
				SQLite.connection.commit();
				SQLite.connection.setAutoCommit(true);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			new Thread(new refreshThread()).start();
		}

		if(e.getSource() == importBBsView.getBtnCancel()){
			importBBsView.dispose();
		}

	}

	public class refreshThread implements Runnable{

		@Override
		public void run() {
			importBBsView.progressBar.setIndeterminate(true);
			if(mainController != null)
				mainController.refreshWindow(true);
			else {
				MainView mainView = new MainView();
				MainController mainController = new MainController(mainView);
				mainView.setController(mainController);
				mainController.refreshWindow(true);
			}
			importBBsView.dispose();
		}

	}

}
