package util;

public class QueryClause {
	
	public static String formatWhereCategories(String arch,
			String instrType){
		String where = "";
		if(!arch.isEmpty())
			where += " architecture = \"" + arch + "\"";
		if(arch.startsWith("ARM")){
			if(instrType.equals("ARM")){
				where += " and (type = \"arm\"" + " or type = \"ARM\")";
			}
			else if(instrType.equals("Thumb")){
				where += " and (type = \"thumb\""+ " or type = \"Thumb\")";
			}
		}
		return where;
	}
	public static String formatWhereCategories(String arch,
			String instrType,String name){
		String where = "";
		if(!arch.isEmpty())
			where += " "+name+"."+"architecture = \"" + arch + "\"";
		if(arch.startsWith("ARM")){
			if(instrType.equals("ARM")){
				where += " and (type = \"arm\"" + " or type = \"ARM\")";
			}
			else if(instrType.equals("Thumb")){
				where += " and (type = \"thumb\""+ " or type = \"Thumb\")";
			}
		}
		return where;
	}
	public static String formatWherePid(int pid,String instrType){
		String where = " pid = ";
		where += String.valueOf(pid);
		if(instrType.equals("ARM")){
			where += " and (type = \"arm\"" + " or type = \"ARM\")";
		}
		else if(instrType.equals("Thumb")){
			where += " and (type = \"thumb\"" + " or type = \"Thumb\")";
		}
		return where;
	}

}
