package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.ui.RefineryUtilities;

import controllers.DeleteArchitectureController;

public class DeleteArchitectureView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	public JComboBox comboBoxArch;
	private JButton okButton;
	private JButton cancelButton;
	
	public DeleteArchitectureView() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 120);
		setTitle("Architecture");
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel lbl = new JLabel("Delete architecture:");
			contentPanel.add(lbl);
		}
		{
			comboBoxArch = new JComboBox();
			contentPanel.add(comboBoxArch);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}
	
	public void setController(DeleteArchitectureController lc){
		this.okButton.addActionListener(lc);
		this.cancelButton.addActionListener(lc);
		
	}
	
	public JButton getBtnOk() {
		return okButton;
	}

	public JButton getBtnCancel() {
		return cancelButton;
	}

	public String getComboBoxArch() {
		return comboBoxArch.getSelectedItem().toString();
	}

}
