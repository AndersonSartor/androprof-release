package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.entity.CategoryLabelEntity;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.SlidingCategoryDataset;

import util.Types;

public class ChartView extends JFrame implements ChangeListener,ChartMouseListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CategoryAxis domainAxis;
	private long maxIdx;
	private JScrollBar scrollBar;
	private SlidingCategoryDataset dataset;
	private int colsPerPage;
	private Types.Elem elem;
    private int pid;
	
	public ChartView(String title, int colsPerPage,int pid) {
		super(title);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.colsPerPage = colsPerPage;
		this.pid = pid;
	}

	public void plotGraph(DefaultCategoryDataset dataset, Types.Elem elem, String graphTitle, String xLabel, String yLabel){
		this.elem = elem;
		maxIdx = dataset.getColumnCount();
		this.dataset = new SlidingCategoryDataset(dataset, 0, colsPerPage);
		JFreeChart chart = createChart(this.dataset, graphTitle, xLabel, yLabel);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.addChartMouseListener(this);
		this.add(chartPanel);
		if(maxIdx<Integer.MAX_VALUE){
			if(colsPerPage>maxIdx)
				scrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, 0, 0, 0);
			else
				scrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, colsPerPage, 0, Integer.parseInt(Long.toString(maxIdx)));
		}
		else
			scrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, colsPerPage, 0, Integer.MAX_VALUE);
		scrollBar.getModel().addChangeListener(this);
		
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		JMenuItem mnHelp = new JMenuItem("Help");
		mnHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new HelpDialogView();
			}
		});
		menuBar.add(mnHelp);
		
		JPanel scrollPanel = new JPanel(new BorderLayout());
		scrollPanel.add(scrollBar);
		scrollPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		this.add(scrollPanel, BorderLayout.SOUTH);
	}

	private JFreeChart createChart(CategoryDataset dataset, String graphTitle, String xLabel, String yLabel) {
		
		JFreeChart chart = ChartFactory.createBarChart(
				graphTitle, 
				xLabel, 
				yLabel, 
				dataset, 
				PlotOrientation.VERTICAL, 
				false, 
				true, 
				true);

		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		domainAxis = (CategoryAxis) plot.getDomainAxis();
		domainAxis.setLowerMargin(0.02);
		domainAxis.setUpperMargin(0.02);
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
		plot.getRangeAxis().setAutoRange(false);

		BarRenderer renderer = (BarRenderer)plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setMaximumBarWidth(0.1f);
		renderer.setSeriesPaint(0, Color.blue);
		
		return chart;

	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		this.dataset.setFirstCategoryIndex(scrollBar.getValue());
	}

	@Override
	public void chartMouseClicked(ChartMouseEvent e) {
		if(e.getEntity().getClass() != CategoryLabelEntity.class){
			return;
		}
		if(elem == Types.Elem.BB)
		{
			try{
				CategoryLabelEntity entity = (CategoryLabelEntity) e.getEntity();
				if(entity != null){
					String key = (String) entity.getKey();
					new BbView(key,pid);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	@Override
	public void chartMouseMoved(ChartMouseEvent arg0) {
		// TODO Auto-generated method stub

	}
}
