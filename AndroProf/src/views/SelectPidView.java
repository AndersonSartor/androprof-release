package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import org.jfree.ui.RefineryUtilities;

import util.Types;

import controllers.BBHistogramChartController;
import controllers.BBCycleCostChartController;
import controllers.BBPowerCostChartController;
import controllers.PerformanceEstimationController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;

public class SelectPidView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField pidTextField;
	private SelectPidView context;
	private JTextField tfColsPerPage;
	/**
	 * Create the dialog.
	 */
	public SelectPidView(final Types.NextView nextView) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		setTitle("PID");
		setBounds(100, 100, 302, 163);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.DEFAULT_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		{
			JLabel lblPid = new JLabel("PID:");
			contentPanel.add(lblPid, "1, 2, left, center");
		}
		{
			pidTextField = new JTextField();
			contentPanel.add(pidTextField, "2, 2, left, top");
			pidTextField.setColumns(10);
		}
		if(nextView != Types.NextView.PERFORMANCE_ESTIMATION){
			{
				JLabel lblColumnsPerPage = new JLabel("Columns per page:");
				contentPanel.add(lblColumnsPerPage, "1, 4, right, default");
			}
			{
				tfColsPerPage = new JTextField();
				contentPanel.add(tfColsPerPage, "2, 4, left, default");
				tfColsPerPage.setColumns(10);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(!pidTextField.getText().isEmpty()){
							try{
								int pid = Integer.parseInt(pidTextField.getText());
								try{
									int colsPerPage = 0;
									if(nextView != Types.NextView.PERFORMANCE_ESTIMATION)
										colsPerPage = Integer.parseInt(tfColsPerPage.getText());
									context.dispose();
									switch(nextView){
									case BB_HISTOGRAM:
										ChartView chartView = new ChartView("BBs Histogram",colsPerPage,pid);
										new BBHistogramChartController(chartView, pid);
										break;
									case BB_TOTAL_CYCLES:
										ChartView chartViewCycles = new ChartView("BBs Total Cycle Cost",colsPerPage, pid);
										new BBCycleCostChartController(chartViewCycles, pid);
										break;
									case BB_TOTAL_POWER:
										ChartView chartViewPower = new ChartView("BBs Total Power Cost",colsPerPage, pid);
										new BBPowerCostChartController(chartViewPower, pid);
										break;
									case PERFORMANCE_ESTIMATION:
										PerformanceEstimationView pv = new PerformanceEstimationView();
										PerformanceEstimationController pc = new PerformanceEstimationController(pv,pid);
										pv.setController(pc);
									default:
										break;
									}
								}
								catch(NumberFormatException ex){
									new ErrorDialogView("Colums per page must be an integer");
								}
							}
							catch(NumberFormatException e ){
								new ErrorDialogView("PID must be an integer");
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						context.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}

}
