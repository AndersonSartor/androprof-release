package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jfree.ui.RefineryUtilities;

import util.Types;

import controllers.PidCycleCostChartController;
import controllers.PidPowerCostChartController;

public class SelectLimitPidsView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField pidTextField;
	private SelectLimitPidsView context;
	/**
	 * Create the dialog.
	 */
	public SelectLimitPidsView(final Types.Cost type) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		setTitle("PID number limit");
		setBounds(100, 100, 300, 154);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel lblPid = new JLabel("Number of PIDs to be shown:");
			contentPanel.add(lblPid);
		}
		{
			pidTextField = new JTextField();
			contentPanel.add(pidTextField);
			pidTextField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						JTextField limitTextField = pidTextField;
						if(!limitTextField.getText().isEmpty()){
							
							try{
								int numberPids = Integer.parseInt(limitTextField.getText());
								context.dispose();
								PidCostChartView pidCostChartView = new PidCostChartView("PID cost Graph");
								switch (type) {
								case CYCLES:
									new PidCycleCostChartController(pidCostChartView, numberPids);	
									break;
								case POWER:
									new PidPowerCostChartController(pidCostChartView, numberPids);
									break;
								default:
									break;
								}
							}
							catch(NumberFormatException e ){
								new ErrorDialogView("The number of PIDs must be an integer");
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						context.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}


}
