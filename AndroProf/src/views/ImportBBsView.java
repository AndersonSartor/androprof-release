package views;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import models.SQLite;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.ImportBBsController;

public class ImportBBsView extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnBrowseBbLog, btnBrowseBbInfo;
	private JPanel panel_1;
	private JPanel panel_3;
	private JButton btnOk;
	private JButton btnCancel;
	public JProgressBar progressBar;
	private JTextField bbLogPath;
	private JTextField bbInfoPath;


	/**
	 * Create the frame.
	 */
	public ImportBBsView() {
		setBounds(100, 100, 400, 300);
		setTitle("Import BBs");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.UNRELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.PARAGRAPH_GAP_ROWSPEC,
				FormFactory.PREF_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("40px"),
				RowSpec.decode("10dlu"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		panel_1 = new JPanel();
		
		bbLogPath = new JTextField();
		contentPane.add(bbLogPath, "2, 3, 5, 1, fill, default");
		bbLogPath.setColumns(10);
		
		JLabel label = new JLabel("BB log file:");
		panel_1.add(label);
		contentPane.add(panel_1, "2, 2, left, fill");
		try {
			Statement stat = SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select path from BbFilesInfo where file=\"bbLog\";");
			if(res.next()){
				bbLogPath.setText(res.getString("path"));
			}
			res.close();
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		btnBrowseBbLog = new JButton("Browse...");
		contentPane.add(btnBrowseBbLog, "7, 3");
		
		bbInfoPath = new JTextField();
		contentPane.add(bbInfoPath, "2, 5, 5, 1, fill, default");
		bbInfoPath.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		
		JLabel label_1 = new JLabel("BB info file:");
		panel_2.add(label_1);
		contentPane.add(panel_2, "2, 4, left, fill");
		try {
			Statement stat = SQLite.connection.createStatement();
			ResultSet res = stat.executeQuery("select path from BbFilesInfo where file=\"bbInfo\";");
			if(res.next()){
				bbInfoPath.setText(res.getString("path"));
			}
			res.close();
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		btnBrowseBbInfo = new JButton("Browse...");
		contentPane.add(btnBrowseBbInfo, "7, 5");
		
		progressBar = new JProgressBar();
		contentPane.add(progressBar, "2, 7, 6, 2");
		
		panel_3 = new JPanel();
		contentPane.add(panel_3, "2, 11, 6, 1, fill, fill");
		
		btnOk = new JButton("OK");
		panel_3.add(btnOk);
		
		btnCancel = new JButton("Cancel");
		panel_3.add(btnCancel);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void setController(ImportBBsController lc){
		this.btnOk.addActionListener(lc);
		this.btnCancel.addActionListener(lc);
		this.btnBrowseBbInfo.addActionListener(lc);
		this.btnBrowseBbLog.addActionListener(lc);
		
	}

	public JButton getBtnLoadBbLog() {
		return btnBrowseBbLog;
	}

	public void setBtnLoadBbLog(JButton btnLoadBbLog) {
		this.btnBrowseBbLog = btnLoadBbLog;
	}

	public JButton getBtnLoadBbInfo() {
		return btnBrowseBbInfo;
	}

	public void setBtnLoadBbInfo(JButton btnLoadBbInfo) {
		this.btnBrowseBbInfo = btnLoadBbInfo;
	}

	public JButton getBtnOk() {
		return btnOk;
	}

	public JButton getBtnCancel() {
		return btnCancel;
	}

	public JTextField getBbLogPath() {
		return bbLogPath;
	}

	public JTextField getBbInfoPath() {
		return bbInfoPath;
	}
}
