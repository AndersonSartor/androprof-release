package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import models.SQLite;

import org.jfree.ui.RefineryUtilities;

public class BbView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private BbView context;
	private String pc;
	private int pid;
	private JTable table;
	private String[] columns = {"PC", "Instruction"};
	private Thread thread;
	/**
	 * Create the dialog.
	 */
	public BbView(String pcX,int pidX) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		this.pc = pcX;
		this.pid = pidX;
		setTitle("BB "+pc);
		setBounds(100, 100, 500, 460);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						context.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		{
			table = new JTable();
			DefaultTableModel model = new DefaultTableModel(
					new Object [][]{},
					columns){
				private static final long serialVersionUID = 1L;
				@Override
				public boolean isCellEditable(int row, int column){
					return false;
				}
			};
			table.setModel(model);
			table.getColumnModel().getColumn(0).setPreferredWidth(100);
			table.getColumnModel().getColumn(1).setPreferredWidth(400);
			scrollPane.setViewportView(table);
		}
		thread = new Thread(new getBb());
		thread.start();
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}

	public class getBb implements Runnable{

		@Override
		public void run() {
			try {
				Statement stat= SQLite.connection.createStatement();
				//System.out.print("select bbInstrs from bbInstrs where idx = (select idx from bb where pc = "+"\""+pc+"\""+" and pid ="+pid+" )");
				ResultSet res = stat.executeQuery("select bbInstrs from bbInstrs where idx = (select idx from bb where pc = "+"\""+pc+"\""+" and pid ="+pid+" )");
				if(res.next()){
					DefaultTableModel dtm = (DefaultTableModel) table.getModel();
					String[] listInstructions = res.getString("bbInstrs").split("\\n");
					for(String instr : listInstructions){
						String instrParts[] = instr.split(":");
						dtm.addRow(new Object[]{instrParts[0], instrParts[1].replaceAll("\t"," ")});
					}
					stat.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
