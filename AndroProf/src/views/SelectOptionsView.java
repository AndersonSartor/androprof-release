package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.SQLite;

import org.jfree.ui.RefineryUtilities;

import util.Types;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.CategoryHistogramController;
import controllers.CategoryCycleCostChartController;
import controllers.CategoryPowerCostChartController;
import controllers.InstructionHistogramController;
import controllers.InstructionCycleCostChartController;
import controllers.InstructionPowerCostChartController;

import javax.swing.JTextField;

public class SelectOptionsView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private SelectOptionsView context;
	private DefaultComboBoxModel cbIntrTypeModel;
	private final JComboBox comboBoxArch;
	private JComboBox comboBoxInstrType;
	private JTextField tfColsPerPage;
	private JTextField tfPid;
	/**
	 * Create the dialog.
	 */
	public SelectOptionsView(final Types.NextView nextView, final double perc) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		setTitle("Options");
		setBounds(100, 100, 479, 200);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		{
			JLabel lblArchitecture = new JLabel("Architecture:");
			contentPanel.add(lblArchitecture, "2, 2, left, default");
		}
		{
			comboBoxArch = new JComboBox();
			comboBoxArch.addItem("Select the architecture");
			try {
				Statement stat = SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select distinct architecture from category");
				while(res.next()){
					comboBoxArch.addItem(res.getString("architecture"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			contentPanel.add(comboBoxArch, "4, 2, fill, default");
			comboBoxArch.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent arg0) {
					String selectedArch = comboBoxArch.getSelectedItem().toString();
					if(selectedArch.toLowerCase().startsWith("arm")){
						cbIntrTypeModel.removeAllElements();
						cbIntrTypeModel.addElement("Both");
						cbIntrTypeModel.addElement("ARM");
						cbIntrTypeModel.addElement("Thumb");
					}
				}

			});
		}
		{
			JLabel lblInstructionType = new JLabel("Instruction type:");
			contentPanel.add(lblInstructionType, "2, 4, left, default");
		}
		{
			comboBoxInstrType = new JComboBox();
			cbIntrTypeModel = (DefaultComboBoxModel) comboBoxInstrType.getModel();
			contentPanel.add(comboBoxInstrType, "4, 4, fill, default");
		}
		{
			JLabel lblColumnPerPage = new JLabel("Columns per page:");
			contentPanel.add(lblColumnPerPage, "2, 6, right, default");
		}
		{
			tfColsPerPage = new JTextField();
			contentPanel.add(tfColsPerPage, "4, 6, fill, default");
			tfColsPerPage.setColumns(10);
		}
		{
			JLabel lblPid = new JLabel("PID:");
			contentPanel.add(lblPid, "2, 8, right, default");
		}
		{
			tfPid = new JTextField();
			contentPanel.add(tfPid, "4, 8, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(!comboBoxArch.getSelectedItem().toString().equals("Select the architecture")){
							try{
								String instrType;
								int colsPerPage = Integer.parseInt(tfColsPerPage.getText());
								int pid = Integer.parseInt(tfPid.getText());
								if(comboBoxInstrType.getSelectedItem() == null)
									instrType = "";
								else
									instrType = comboBoxInstrType.getSelectedItem().toString();

								context.dispose();
								switch (nextView) {
								case INSTRUCTION_HISTOGRAM:
									ChartView instructionHistogramGraphView = new ChartView("Instructions Histogram",colsPerPage,pid);
									
									new InstructionHistogramController(instructionHistogramGraphView, comboBoxArch.getSelectedItem().toString(),
											pid, instrType, perc);
									break;
								case INSTRUCTION_TOTAL_CYCLE:
									ChartView instructionTotalCycleCostGraphView = new ChartView("Instructions Total Cycle Cost",colsPerPage, pid);
									new InstructionCycleCostChartController(instructionTotalCycleCostGraphView, comboBoxArch.getSelectedItem().toString(),
											instrType, pid, perc);
									break;
								case INSTRUCTION_TOTAL_POWER:
									ChartView instructionTotalPowerCostGraphView = new ChartView("Instructions Total Power Cost",colsPerPage, pid);
									new InstructionPowerCostChartController(instructionTotalPowerCostGraphView, comboBoxArch.getSelectedItem().toString(),
											instrType, pid, perc);
									break;
								case CATEGORY_HISTOGRAM:
									ChartView categoryGraphView = new ChartView("Categories Histogram",colsPerPage, pid);
									new CategoryHistogramController(categoryGraphView, comboBoxArch.getSelectedItem().toString(),
											instrType, pid);
									break;
								case CATEGORY_TOTAL_CYCLES:
									ChartView categoryCycleGraphView = new ChartView("Categories Total Cycle Cost",colsPerPage, pid);
									new CategoryCycleCostChartController(categoryCycleGraphView, comboBoxArch.getSelectedItem().toString(),
											instrType, pid);
									break;
								case CATEGORY_TOTAL_POWER:
									ChartView categoryPowerGraphView = new ChartView("Categories Total Power Cost",colsPerPage, pid);
									new CategoryPowerCostChartController(categoryPowerGraphView, comboBoxArch.getSelectedItem().toString(),
											instrType, pid);
									break;
								default:
									break;
								}
							}
							catch(NumberFormatException e){
								new ErrorDialogView("Invalid number of columns per page");
							}
						}
						else
							new ErrorDialogView("The architecture must be selected");
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						context.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}

}
