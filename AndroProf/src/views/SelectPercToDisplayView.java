package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jfree.ui.RefineryUtilities;

import util.Types;

public class SelectPercToDisplayView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField percTextField;
	private SelectPercToDisplayView context;
	/**
	 * Create the dialog.
	 */
	public SelectPercToDisplayView(final Types.NextView nextView) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		setTitle("Display percentage");
		setBounds(100, 100, 300, 152);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel lblDisplay = new JLabel("Display the");
			contentPanel.add(lblDisplay);
		}
		{
			percTextField = new JTextField();
			contentPanel.add(percTextField);
			percTextField.setColumns(3);
		}
		{
			JLabel label = new JLabel("%");
			contentPanel.add(label);
		}
		{
			JLabel lblMore = new JLabel("most significant instructions");
			contentPanel.add(lblMore);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(!percTextField.getText().isEmpty()){
							try{
								double perc = Double.parseDouble(percTextField.getText());
								if(perc<0 || perc>100)
									throw new NumberFormatException();
								context.dispose();
								switch(nextView){
								case INSTRUCTION_HISTOGRAM:
									new SelectOptionsView(Types.NextView.INSTRUCTION_HISTOGRAM,100-perc);
									break;
								case INSTRUCTION_TOTAL_CYCLE:
									new SelectOptionsView(Types.NextView.INSTRUCTION_TOTAL_CYCLE,100-perc);
									break;
								case INSTRUCTION_TOTAL_POWER:
									new SelectOptionsView(Types.NextView.INSTRUCTION_TOTAL_POWER,100-perc);
									break;
								default:
									break;
								}
							}
							catch(NumberFormatException e ){
								new ErrorDialogView("Invalid percetage");
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						context.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}

}
