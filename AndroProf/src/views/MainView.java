package views;

import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.MainController;

public class MainView  extends JFrame{

	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JScrollPane scrollPane;
	public JTable table;
	private String[] columns = {"PID", "BB ID", "PC", "Cycle Cost", "Power Cost", "BB Counter", 
			"Total Cycle Cost", "Total Power Cost"};
	private String[] benchmarkColumns = {"Benchmark", "PID"};
	private JPanel panel;
	private JButton btnPrevPage;
	private JButton btnNextPage;
	private JMenu mnGraphs;
	private JMenuItem mntmCycleCostPid;
	private JMenu mnImport;
	private JMenuItem mntmImportBbFiles;
	private JMenuItem mntmBbHistogram;
	private JMenuItem mntmInstructionHistogram;
	private JMenuItem mntmInstructionTotalCycles;
	private JMenuItem mntmBbTotalCycle;
	private JMenuItem mntmCategoryHistogram;
	private JMenuItem mntmCategoryTotalCycles;
	private JMenu mnEdit;
	private JMenuItem mntmDeleteArchitecture;
	private JMenuItem mntmSelectArchitecture;
	private JLabel lblFilters;
	private JPanel panel_1;
	private JLabel lblPid;
	private JTextField pidFilter;
	private JLabel lblBbId;
	private JTextField bbIdFilter;
	private JPanel panel_2;
	private JPanel panel_3;
	private JLabel lblPreviousPage;
	private JLabel lblNextPage;
	private JButton btnFilter;
	private Component horizontalStrut;
	private JSeparator separator;
	private JMenuItem mntmBbsTotalPower;
	private JMenuItem mntmCategoriesTotalPower;
	private JMenuItem mntmInstructionsTotalPower;
	private JMenuItem mntmPowerCostPid;
	private JMenu mnEstimation;
	private JMenuItem mntmPerformance;
	private JTextField bbLogPath;
	private JTextField bbInfoPath;
	private JMenuItem mntmImportExecutedBenchmarks;
	private JMenuItem mntmDeleteExecutedBenchmarks;
	private JMenuItem mntmBenchmarks;
	private JScrollPane benchmarksScrollPane;
	private JTable benchmarksTable;

	/**
	 * Initialize the contents of the frame.
	 */
	public MainView() {
		super("BB Reader");
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 600);
		frame.setTitle("AndroProf");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		mnImport = new JMenu("Import");
		menuBar.add(mnImport);

		mntmImportBbFiles = new JMenuItem("Import BB files");
		mnImport.add(mntmImportBbFiles);
		
		mntmImportExecutedBenchmarks = new JMenuItem("Import executed benchmarks");
		mnImport.add(mntmImportExecutedBenchmarks);

		mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);

		mntmSelectArchitecture = new JMenuItem("Select architecture");
		mnEdit.add(mntmSelectArchitecture);

		mntmDeleteArchitecture = new JMenuItem("Delete architecture");
		mnEdit.add(mntmDeleteArchitecture);
		
		mntmDeleteExecutedBenchmarks = new JMenuItem("Delete executed benchmarks");
		mnEdit.add(mntmDeleteExecutedBenchmarks);
		
		mnEstimation = new JMenu("Estimation");
		menuBar.add(mnEstimation);
		
		mntmPerformance = new JMenuItem("Performance and power estimation");
		mnEstimation.add(mntmPerformance);

		mnGraphs = new JMenu("Charts");
		menuBar.add(mnGraphs);

		mntmCycleCostPid = new JMenuItem("Cycle cost by PID");
		mnGraphs.add(mntmCycleCostPid);
		
		mntmPowerCostPid = new JMenuItem("Power cost by PID");
		mnGraphs.add(mntmPowerCostPid);

		mntmCategoryHistogram = new JMenuItem("Instruction categories histogram");
		mnGraphs.add(mntmCategoryHistogram);

		mntmCategoryTotalCycles = new JMenuItem("Instruction categories total cycle cost");
		mnGraphs.add(mntmCategoryTotalCycles);
		
		mntmCategoriesTotalPower = new JMenuItem("Instruction categories total power cost");
		mnGraphs.add(mntmCategoriesTotalPower);

		mntmBbHistogram = new JMenuItem("BBs histogram");
		mnGraphs.add(mntmBbHistogram);

		mntmBbTotalCycle = new JMenuItem("BBs total cycle cost");
		mnGraphs.add(mntmBbTotalCycle);
		
		mntmBbsTotalPower = new JMenuItem("BBs total power cost");
		mnGraphs.add(mntmBbsTotalPower);

		mntmInstructionHistogram = new JMenuItem("Instructions histogram");
		mnGraphs.add(mntmInstructionHistogram);

		mntmInstructionTotalCycles = new JMenuItem("Instructions total cycle cost");
		mnGraphs.add(mntmInstructionTotalCycles);
		
		mntmInstructionsTotalPower = new JMenuItem("Instructions total power cost");
		mnGraphs.add(mntmInstructionsTotalPower);
		
		mntmBenchmarks = new JMenuItem("Benchmarks");
		menuBar.add(mntmBenchmarks);
		frame.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("5dlu"),
				FormFactory.DEFAULT_COLSPEC,
				ColumnSpec.decode("max(165dlu;default)"),
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("15px:grow"),
				RowSpec.decode("fill:default"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("default:grow"),
				RowSpec.decode("fill:default"),}));

		JLabel lblBbLogFile = new JLabel("BB Log File:");
		frame.getContentPane().add(lblBbLogFile, "2, 2");
		
		bbLogPath = new JTextField();
		bbLogPath.setEditable(false);
		frame.getContentPane().add(bbLogPath, "3, 2, 2, 1, fill, default");
		bbLogPath.setColumns(10);

		JLabel lblBbInfoFile = new JLabel("BB Info File:");
		frame.getContentPane().add(lblBbInfoFile, "2, 3");
		
		bbInfoPath = new JTextField();
		bbInfoPath.setEditable(false);
		frame.getContentPane().add(bbInfoPath, "3, 3, 2, 1, fill, default");
		bbInfoPath.setColumns(10);

		horizontalStrut = new JSeparator(SwingConstants.HORIZONTAL);
		frame.getContentPane().add(horizontalStrut, "1, 5, 4, 1");

		lblFilters = new JLabel("Filters:");
		frame.getContentPane().add(lblFilters, "2, 6, default, bottom");
		
		benchmarksScrollPane = new JScrollPane();
		frame.getContentPane().add(benchmarksScrollPane, "4, 6, 1, 3, fill, fill");
		
		benchmarksTable = new JTable();
		DefaultTableModel benchmarkModel = new DefaultTableModel(
				new Object [][]{},
				benchmarkColumns){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		benchmarksTable.setModel(benchmarkModel);
		benchmarksScrollPane.setViewportView(benchmarksTable);

		panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, "2, 7, 2, 1, fill, fill");
		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				ColumnSpec.decode("50dlu"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		lblPid = new JLabel("PID = ");
		panel_1.add(lblPid, "2, 2, right, default");

		pidFilter = new JTextField();
		panel_1.add(pidFilter, "3, 2, fill, default");
		pidFilter.setColumns(10);

		lblBbId = new JLabel("BB ID = ");
		panel_1.add(lblBbId, "5, 2, right, default");

		bbIdFilter = new JTextField();
		panel_1.add(bbIdFilter, "7, 2, fill, default");
		bbIdFilter.setColumns(10);

		btnFilter = new JButton("Filter");
		panel_1.add(btnFilter, "9, 2");

		separator = new JSeparator(SwingConstants.HORIZONTAL);
		frame.getContentPane().add(separator, "1, 9, 4, 1");

		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		frame.getContentPane().add(scrollPane, "1, 10, 4, 1, fill, fill");

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DefaultTableModel model = new DefaultTableModel(
				new Object [][]{},
				columns){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		table.setModel(model);
		TableRowSorter<DefaultTableModel> rowSorter = new TableRowSorter<DefaultTableModel>(model){
			@Override
			public void sort(){

			}
		};
		List<SortKey> sortKeys = new ArrayList<SortKey>();
		sortKeys.add(new RowSorter.SortKey(6, SortOrder.DESCENDING));
		rowSorter.setSortKeys(sortKeys);
		table.setRowSorter(rowSorter);
		scrollPane.setViewportView(table);

		panel = new JPanel();
		frame.getContentPane().add(panel, "2, 11, 3, 1, fill, fill");
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnPrevPage = new JButton("<");
		panel_2.add(btnPrevPage);

		lblPreviousPage = new JLabel("Previous page");
		panel_2.add(lblPreviousPage);

		panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		lblNextPage = new JLabel("Next page");
		panel_3.add(lblNextPage);

		btnNextPage = new JButton(">");
		panel_3.add(btnNextPage);
		this.frame.setLocationRelativeTo(null);
		this.frame.setVisible(true);
	}

	public void setController(MainController mc){
		this.addWindowListener(mc);
		this.mntmImportBbFiles.addActionListener(mc);
		this.mntmCycleCostPid.addActionListener(mc);
		this.mntmPowerCostPid.addActionListener(mc);
		this.table.getRowSorter().addRowSorterListener(mc.rowSorterListener);
		this.btnPrevPage.addActionListener(mc);
		this.btnNextPage.addActionListener(mc);
		this.mntmBbHistogram.addActionListener(mc);
		this.mntmInstructionHistogram.addActionListener(mc);
		this.mntmInstructionTotalCycles.addActionListener(mc);
		this.mntmBbTotalCycle.addActionListener(mc);
		this.mntmCategoryHistogram.addActionListener(mc);
		this.mntmCategoryTotalCycles.addActionListener(mc);
		this.mntmDeleteArchitecture.addActionListener(mc);
		this.mntmSelectArchitecture.addActionListener(mc);
		this.btnFilter.addActionListener(mc);
		this.mntmInstructionsTotalPower.addActionListener(mc);
		this.mntmBbsTotalPower.addActionListener(mc);
		this.mntmCategoriesTotalPower.addActionListener(mc);
		this.mntmPerformance.addActionListener(mc);
		this.mntmImportExecutedBenchmarks.addActionListener(mc);
		this.mntmDeleteExecutedBenchmarks.addActionListener(mc);
		this.mntmBenchmarks.addActionListener(mc);
	}

	public String[] getColumns() {
		return columns;
	}

	public JButton getBtnPrevPage() {
		return btnPrevPage;
	}

	public JButton getBtnNextPage() {
		return btnNextPage;
	}

	public JMenuItem getMntmCycleCostPid() {
		return mntmCycleCostPid;
	}

	public JMenuItem getMntmImportBbFiles() {
		return mntmImportBbFiles;
	}

	public JMenuItem getMntmBbHistogram() {
		return mntmBbHistogram;
	}

	public JMenuItem getMntmInstructionHistogram() {
		return mntmInstructionHistogram;
	}

	public JMenuItem getMntmInstructionTotalCycles() {
		return mntmInstructionTotalCycles;
	}

	public JMenuItem getMntmBbTotalCycle() {
		return mntmBbTotalCycle;
	}

	public JMenuItem getMntmCategoryHistogram() {
		return mntmCategoryHistogram;
	}

	public JMenuItem getMntmCategoryTotalCycles() {
		return mntmCategoryTotalCycles;
	}

	public JMenuItem getMntmDeleteArchitecture() {
		return mntmDeleteArchitecture;
	}

	public JMenuItem getMntmSelectArchitecture() {
		return mntmSelectArchitecture;
	}

	public JButton getBtnFilter() {
		return btnFilter;
	}

	public JTextField getPidFilter() {
		return pidFilter;
	}

	public JTextField getBbIdFilter() {
		return bbIdFilter;
	}

	public JMenuItem getMntmBbsTotalPower() {
		return mntmBbsTotalPower;
	}

	public JMenuItem getMntmCategoriesTotalPower() {
		return mntmCategoriesTotalPower;
	}

	public JMenuItem getMntmInstructionsTotalPower() {
		return mntmInstructionsTotalPower;
	}

	public JMenuItem getMntmPowerCostPid() {
		return mntmPowerCostPid;
	}

	public JMenuItem getMntmPerformance() {
		return mntmPerformance;
	}

	public JTextField getBbLogPath() {
		return bbLogPath;
	}

	public JTextField getBbInfoPath() {
		return bbInfoPath;
	}

	public JMenuItem getMntmImportExecutedBenchmarks() {
		return mntmImportExecutedBenchmarks;
	}

	public JMenuItem getMntmDeleteExecutedBenchmarks() {
		return mntmDeleteExecutedBenchmarks;
	}

	public JMenuItem getMntmBenchmarks() {
		return mntmBenchmarks;
	}

	public JTable getBenchmarksTable() {
		return benchmarksTable;
	}

	public JScrollPane getBenchmarksScrollPane() {
		return benchmarksScrollPane;
	}


}
