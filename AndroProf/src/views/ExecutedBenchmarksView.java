package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import models.SQLite;

import org.jfree.ui.RefineryUtilities;

public class ExecutedBenchmarksView extends JFrame {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private ExecutedBenchmarksView context;
	private JTable table;
	private String[] columns = {"Benchmark", "PID", "Execution time(ms)"};
	private Thread thread;
	/**
	 * Create the dialog.
	 */
	public ExecutedBenchmarksView() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		setTitle("Executed benchmarks");
		setBounds(100, 100, 500, 460);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						context.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		{
			table = new JTable();
			DefaultTableModel model = new DefaultTableModel(
					new Object [][]{},
					columns){
				private static final long serialVersionUID = 1L;
				@Override
				public boolean isCellEditable(int row, int column){
					return false;
				}
			};
			table.setModel(model);
			scrollPane.setViewportView(table);
		}
		thread = new Thread(new GetBenchmarks());
		thread.start();
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}

	public class GetBenchmarks implements Runnable{

		@Override
		public void run() {
			try {
				Statement stat= SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select * from benchmark");
				DefaultTableModel dtm = (DefaultTableModel) table.getModel();
				while(res.next()){
					String benchmark = res.getString("name");
					int pid = res.getInt("pid");
					long time = res.getLong("time");
					dtm.addRow(new Object[]{benchmark, pid, time});
				}
				res.close();
				stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
