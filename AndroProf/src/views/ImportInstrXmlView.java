package views;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jfree.ui.RefineryUtilities;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.ImportExecutedBenchmarksController;
import controllers.ImportInstrXmlController;

public class ImportInstrXmlView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnBrowseXmlFIle;
	private JPanel panel_1;
	private JPanel panel_3;
	private JButton btnOk;
	private JButton btnCancel;
	private JProgressBar progressBar;
	private JTextField xmlFilePath;
	private JLabel lblInstructionsXmlFile;

	/**
	 * Create the frame.
	 */
	public ImportInstrXmlView() {
		setBounds(100, 100, 402, 227);
		setTitle("Import XML");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.UNRELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.PARAGRAPH_GAP_ROWSPEC,
				FormFactory.PREF_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("40px"),
				RowSpec.decode("10dlu"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		panel_1 = new JPanel();
		
		lblInstructionsXmlFile = new JLabel("Instructions XML File:");
		panel_1.add(lblInstructionsXmlFile);
		contentPane.add(panel_1, "2, 2, left, fill");
		
		xmlFilePath = new JTextField();
		xmlFilePath.setEditable(false);
		contentPane.add(xmlFilePath, "2, 3, 5, 1, fill, default");
		xmlFilePath.setColumns(10);
		
		btnBrowseXmlFIle = new JButton("Browse...");
		contentPane.add(btnBrowseXmlFIle, "7, 3");
		
		progressBar = new JProgressBar();
		contentPane.add(progressBar, "2, 5, 6, 2");
		
		panel_3 = new JPanel();
		contentPane.add(panel_3, "2, 9, 6, 1, fill, fill");
		
		btnOk = new JButton("OK");
		panel_3.add(btnOk);
		
		btnCancel = new JButton("Cancel");
		panel_3.add(btnCancel);
		RefineryUtilities.centerFrameOnScreen(this);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	public void setController(ImportInstrXmlController lc){
		this.btnOk.addActionListener(lc);
		this.btnCancel.addActionListener(lc);
		this.btnBrowseXmlFIle.addActionListener(lc);
	}
	
	public void setControllerBenchmarks(ImportExecutedBenchmarksController lc){
		this.btnOk.addActionListener(lc);
		this.btnCancel.addActionListener(lc);
		this.btnBrowseXmlFIle.addActionListener(lc);
	}

	public JButton getBtnBrowseXmlFIle() {
		return btnBrowseXmlFIle;
	}

	public void setBtnBrowseXmlFIle(JButton btnLoadBbLog) {
		this.btnBrowseXmlFIle = btnLoadBbLog;
	}

	public JButton getBtnOk() {
		return btnOk;
	}

	public JButton getBtnCancel() {
		return btnCancel;
	}

	public JTextField getXmlFilePath() {
		return xmlFilePath;
	}

	public void setXmlFilePath(JTextField xmlFilePath) {
		this.xmlFilePath = xmlFilePath;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setLblInstructionsXmlFile(String lblInstructionsXmlFile) {
		this.lblInstructionsXmlFile.setText(lblInstructionsXmlFile);
	}

}
