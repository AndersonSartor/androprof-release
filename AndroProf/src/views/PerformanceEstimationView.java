package views;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.PerformanceEstimationController;

public class PerformanceEstimationView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField operationFrequency;
	private JButton btnCalculate;
	private JTextField estimTime;
	private JLabel lblProcessorPowerDissipation;
	private JTextField procPwDis;
	private JLabel lblCpi;
	private JTextField cpi;
	private JLabel lblNumberOfInstructions;
	private JTextField baseNumInstructions;
	private JLabel lblNumberOfCache;
	private JTextField baseNumCacheMisses;
	private JLabel lblEstimatedPowerDissipation;
	private JTextField estimPowerDis;
	private JLabel lblEstimatedEnergyConsumption;
	private JTextField estimEnergyCons;
	private JLabel lblResults;
	private JTextField memAcEnCons;
	private JLabel lblNewLabel_1;

	public PerformanceEstimationView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 513, 467);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JLabel lblNewLabel = new JLabel("Operation frequency (GHz):");
		contentPane.add(lblNewLabel, "2, 2, left, default");

		operationFrequency = new JTextField();
		contentPane.add(operationFrequency, "4, 2, fill, default");
		operationFrequency.setColumns(10);

		lblProcessorPowerDissipation = new JLabel("Processor power dissipation (W):");
		contentPane.add(lblProcessorPowerDissipation, "2, 4, left, default");

		procPwDis = new JTextField();
		contentPane.add(procPwDis, "4, 4, fill, default");
		procPwDis.setColumns(10);

		lblCpi = new JLabel("CPI:");
		contentPane.add(lblCpi, "2, 6, left, default");

		cpi = new JTextField();
		contentPane.add(cpi, "4, 6, fill, default");
		cpi.setColumns(10);

		lblNumberOfInstructions = new JLabel("Base number of instructions:");
		contentPane.add(lblNumberOfInstructions, "2, 8, left, default");

		baseNumInstructions = new JTextField();
		contentPane.add(baseNumInstructions, "4, 8, fill, default");
		baseNumInstructions.setColumns(10);

		lblNumberOfCache = new JLabel("Base number of cache misses:");
		contentPane.add(lblNumberOfCache, "2, 10, left, default");

		baseNumCacheMisses = new JTextField();
		contentPane.add(baseNumCacheMisses, "4, 10, fill, default");
		baseNumCacheMisses.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Memory access memory consumption (nJ):");
		contentPane.add(lblNewLabel_1, "2, 12, right, default");
		
		memAcEnCons = new JTextField();
		contentPane.add(memAcEnCons, "4, 12, fill, default");
		memAcEnCons.setColumns(10);

		lblResults = new JLabel("Results");
		contentPane.add(lblResults, "1, 16, 2, 1");

		JLabel lblTimeEstimation = new JLabel("Time estimation (s):");
		contentPane.add(lblTimeEstimation, "2, 18, left, default");

		estimTime = new JTextField();
		estimTime.setEditable(false);
		contentPane.add(estimTime, "4, 18, fill, default");
		estimTime.setColumns(10);

		lblEstimatedPowerDissipation = new JLabel("Estimated power dissipation (W):");
		contentPane.add(lblEstimatedPowerDissipation, "2, 20, left, default");

		estimPowerDis = new JTextField();
		estimPowerDis.setEditable(false);
		contentPane.add(estimPowerDis, "4, 20, fill, default");
		estimPowerDis.setColumns(10);

		lblEstimatedEnergyConsumption = new JLabel("Estimated energy consumption (J):");
		contentPane.add(lblEstimatedEnergyConsumption, "2, 22, left, default");

		estimEnergyCons = new JTextField();
		estimEnergyCons.setEditable(false);
		contentPane.add(estimEnergyCons, "4, 22, fill, default");
		estimEnergyCons.setColumns(10);

		JPanel panel = new JPanel();
		contentPane.add(panel, "2, 24, 3, 1, fill, fill");

		btnCalculate = new JButton("Calculate");
		panel.add(btnCalculate);

		setLocationRelativeTo(null);
		setVisible(true);
	}

	public void setController(PerformanceEstimationController pc){
		this.btnCalculate.addActionListener(pc);
	}

	public JButton getBtnCalculate() {
		return btnCalculate;
	}

	public double getOperationFrequency() {
		try{
			return Double.parseDouble(operationFrequency.getText().toString());
		}
		catch(NumberFormatException e){
			new ErrorDialogView("Invalid operation frequency");
			return 0;
		}
	}

	public void setOperationFrequency(double operationFrequency) {
		this.operationFrequency.setText(String.valueOf(operationFrequency));
	}

	public JTextField getResult() {
		return estimTime;
	}

	public void setResult(double result) {
		this.estimTime.setText(String.valueOf(result));
	}

	public JTextField getEstimPowerDis() {
		return estimPowerDis;
	}

	public void setEstimPowerDis(double estimPowerDis) {
		this.estimPowerDis.setText(String.valueOf(estimPowerDis));
	}

	public JTextField getEstimEnergyCons() {
		return estimEnergyCons;
	}

	public void setEstimEnergyCons(double estimEnergyCons) {
		this.estimEnergyCons.setText(String.valueOf(estimEnergyCons));
	}

	public double getProcPwDis() {
		try{
			return Double.valueOf(procPwDis.getText());
		}
		catch(NumberFormatException e ){
			return 0;
		}
	}

	public double getCpi() {
		try{
			return Double.valueOf(cpi.getText());
		}
		catch(NumberFormatException e ){
			return 0;
		}
	}

	public long getBaseNumInstructions() {
		try{
			return Long.valueOf(baseNumInstructions.getText());
		}
		catch(NumberFormatException e ){
			return 0;
		}
	}

	public long getBaseNumCacheMisses() {
		try{
			return Long.valueOf(baseNumCacheMisses.getText());
		}
		catch(NumberFormatException e ){
			return 0;
		}
	}

	public double getMemAcEnCons() {
		try{
			return Double.valueOf(memAcEnCons.getText());
		}
		catch(NumberFormatException e ){
			return 0;
		}
	}

}
