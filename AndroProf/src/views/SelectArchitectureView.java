package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.SQLite;

import org.jfree.ui.RefineryUtilities;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import controllers.ImportBBsController;
import controllers.ImportInstrXmlController;
import controllers.MainController;

public class SelectArchitectureView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private SelectArchitectureView context;
	public static JComboBox comboBoxArch;
	/**
	 * Create the dialog.
	 */
	public SelectArchitectureView(final MainController mainController) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		context=this;
		setTitle("Architecture");
		setBounds(100, 100, 318, 146);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		{
			JLabel lblArchitecture = new JLabel("Architecture:");
			contentPanel.add(lblArchitecture, "2, 2, left, default");
		}
		{
			comboBoxArch = new JComboBox();
			contentPanel.add(comboBoxArch, "4, 2, fill, default");
			try {
				Statement stat = SQLite.connection.createStatement();
				ResultSet res = stat.executeQuery("select distinct architecture from category");
				while(res.next()){
					comboBoxArch.addItem(res.getString("architecture"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			{
				JButton btnImportInstrXml = new JButton("Import Instr. XML");
				btnImportInstrXml.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ImportInstrXmlView importInstrXmlView = new ImportInstrXmlView();
						ImportInstrXmlController importInstrXmlController = new ImportInstrXmlController(importInstrXmlView, true);
						importInstrXmlView.setController(importInstrXmlController);
					}
				});
				contentPanel.add(btnImportInstrXml, "2, 4");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(!(comboBoxArch.getSelectedItem() == null)){
							context.dispose();
							
								try {
									Statement stat = SQLite.connection.createStatement();
									ResultSet res = stat.executeQuery("select value from architecture where arch=\"selectedArch\";");
									if(res.next()){
										if(!comboBoxArch.getSelectedItem().toString().equals(res.getString("value"))){
											try {
												Statement statIns = SQLite.connection.createStatement();
												statIns.executeUpdate("insert or replace into architecture values(\"selectedArch\"," +
														"\""+comboBoxArch.getSelectedItem().toString()+"\");");
											} catch (SQLException e) {
												e.printStackTrace();
											}
											ImportBBsView loadBBsView = new ImportBBsView();
											ImportBBsController loadBBsController = new ImportBBsController(loadBBsView, mainController);
											loadBBsView.setController(loadBBsController);
											loadBBsView.setAlwaysOnTop(true);
										}
										else {
											MainView mainView = new MainView();
											MainController mainController = new MainController(mainView);
											mainView.setController(mainController);
										}
									}
									res.close();
									stat.close();
								} catch (SQLException e) {
									e.printStackTrace();
								}
						}
						else
							new ErrorDialogView("Import the instruction's XML of the desired architecture before continuing.");
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						context.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		RefineryUtilities.centerFrameOnScreen(this);
		setVisible(true);
	}

}
