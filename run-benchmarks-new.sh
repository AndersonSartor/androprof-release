#!/bin/bash

source variables.sh

echo "Benchmarks Java:"
echo "1 - scimark fft"
echo "2 - scimark lu"
echo "3 - scimark monte carlo"
echo "4 - scimark sor"
echo "5 - scimark sparse"
echo "6 - serial"
echo "7 - crypto aes"
echo "8 - crypto rsa"
echo "9 - crypto signverify"
echo "10 - compress"
echo "11 - mpegaudio"
echo "12 - sqlite"
echo "-----------------------"
echo ""
echo "Android folder:"
read androidFolder

adb_cmd="./adb shell am start -n com.android.spec."
activity="/com.android.spec.SpecActivity"
cmd=""

rm ./$androidTraces/$timeInfoFile
rm ./$androidTraces/$finalTimeFile
./adb logcat > $logcatFile &
mkdir -p $androidTraces

for i in {1..12}
do
	case $i in
		1) cmd=$adb_cmd"fft"$activity;;
		2) cmd=$adb_cmd"lu"$activity;;
		3) cmd=$adb_cmd"montecarlo"$activity;;
		4) cmd=$adb_cmd"sor"$activity;;
		5) cmd=$adb_cmd"sparse"$activity;;
		6) cmd=$adb_cmd"serial"$activity;;
		7) cmd=$adb_cmd"aes"$activity;;
		8) cmd=$adb_cmd"rsa"$activity;;
		9) cmd=$adb_cmd"signverify"$activity;;
		10) cmd=$adb_cmd"compress"$activity;;
		11) cmd=$adb_cmd"mpegaudio"$activity;;
		12) cmd="./adb shell am start -n org.garret.bench/org.garret.bench.Benchmark";;
		*) ;;
	esac
	echo "Starting benchmark $i"
	eval $cmd
	tail -f $logcatFile | grep -m 1 "Benchmark finished" >> ./$androidTraces/$timeInfoFile | xargs echo "" >> $logcatFile \;
	#./adb shell input keyevent 4
	echo "Benchmark $i finished"
	sleep 10
done
rm $logcatFile
echo "Done... After closing the VNC connection, run copy-files.sh"



